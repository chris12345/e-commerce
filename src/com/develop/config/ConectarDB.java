package com.develop.config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConectarDB {
	public static final String URL = "jdbc:mysql://localhost:3306/db_ecomerce";
	public static final String USER = "root";
	public static final String CLAVE = "";
	Connection conn;
	/*public ConectarDB() throws ClassNotFoundException, SQLException {
		//try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = (Connection) DriverManager.getConnection(URL,USER,CLAVE);
		//} catch (Exception e) {
			//System.out.println("Primer catch:"+e.getMessage());
		//}
	}*/
	
	public Connection conectarDB() throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		return conn = (Connection) DriverManager.getConnection(URL,USER,CLAVE);
	}
	
	/*public Connection getConnection() {
		return conn;
	}*/
}
