package com.develop.interfaces;

import java.util.ArrayList;

import com.develop.models.ProductoM;

public interface ProductoI {
	public ArrayList<ProductoM> verProductos();
	public int insertarProducto(String nombreProducto, String descripcion,double precioUnitario, String tipoDescuento, String tipoCategoria,int disponibilidad, String marca);
	public int actualizarProducto(int idProducto, String nombre, String descripcion, double precioUnitario, String tipoDescuento, String tipoCategoria, int disponibilidad, String marca);
	public int borrarProducto(int idProducto);
	public ArrayList<ProductoM> filtrarProducto(String tipoCategoria);
}
