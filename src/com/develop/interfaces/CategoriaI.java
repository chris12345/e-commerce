package com.develop.interfaces;

import java.util.ArrayList;

import com.develop.models.CategoriaM;

public interface CategoriaI {
	public ArrayList<CategoriaM> verCategorias();
	public int borrarCategoria(int idCategoria);
	public int crearCategoria(String tipoCategoria, String descripcionCategoria);
	public int actualizarCategoria(int idCategoria, String tipoCategoria, String descripcionCategoria);
}
