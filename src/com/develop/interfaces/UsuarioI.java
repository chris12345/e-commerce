package com.develop.interfaces;

import java.sql.SQLException;
import java.util.ArrayList;

import com.develop.models.UsuarioM;

public interface UsuarioI {
	public String validarUsuario(String nombre, String password) throws ClassNotFoundException, SQLException;
	public ArrayList<UsuarioM> verUsuario();
	public int crearUsuario(String nombre, String apePat, String apeMat, String direccion, String password, String tipoUsuario, String telefono, String email, String cp, String ciudad);
	public int borrarUsuario(int idUsuario);
	public int actualizarUsuario(int idUsuario, String nombre, String apellidoPaterno, String apellidoMaterno, String direccion, String password, String tipoUsuario, String telefono, String email, String codigoPostal,String ciudad);
}
