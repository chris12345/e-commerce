package com.develop.interfaces;

import java.util.ArrayList;

import com.develop.models.DescuentoM;

public interface DescuentoI {
	public ArrayList<DescuentoM> verDescuento();
	public int borrarDescuento(int idDescuento);
	public int crearDescuento(String tipoDescuento,String fechaInicio, String fechafin, double cantidadDescuento);
	public int actualizarDescuento(int idDescuento, String tipoDescuento, String fechaInicio, String fechafin, double cantidadDescuento);
}
