package com.develop.interfaces;

import java.util.ArrayList;

import com.develop.models.PedidoM;

public interface PedidoI {
	public ArrayList<PedidoM> verPedido();
	public int crearPedido(String referencia, String nombreProducto, double precioUnitario, int cantidad,String nombreUsuario, String fechaCreacion, String fechaExpiracion,String statusCompra, String statusPedido);
	public int borrarPedido(int idPedido);
	public int actualizarPedido(int idPedido, String referencia, String nombreProducto, double precioUnitario, int cantidad,String nombreUsuario, String fechaCreacion, String fechaExpiracion,String statusCompra, String statusPedido);
	public ArrayList<PedidoM> filtrarPedido(String nombreUsuario);
	public int actualizarPedidoCarrito(String nombreUsuario);
	public int cancelarPedidoCarrito(String nombreUsuario);
	public ArrayList<PedidoM> filtroParaEncargado();
	public ArrayList<PedidoM> filtroProductoEncargado(String referenciaPedido);
	public int actualizarPedidoEncargado(String referencia, String statusPedido);
	}