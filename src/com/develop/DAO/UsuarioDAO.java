package com.develop.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.develop.config.ConectarDB;
import com.develop.interfaces.UsuarioI;
import com.develop.models.UsuarioM;

public class UsuarioDAO implements UsuarioI {
	ConectarDB conectardb =  new ConectarDB();
	Connection con;
	PreparedStatement ps;
	ResultSet rs;

	@Override
	public String validarUsuario(String nombre, String password) throws ClassNotFoundException, SQLException {
		String consulta = "select tipoUsuario from usuario where nombre='" + nombre + "' and password='" + password + "'";
		String tipoUsuario = "";
		conectardb = new ConectarDB();
		con = conectardb.conectarDB();
		//con = conectardb.getConnection();
		try {
			ps = con.prepareStatement(consulta);
			rs = ps.executeQuery();
			if (rs.next()) {
				tipoUsuario = rs.getString("tipoUsuario");
				System.out.println("Tipo usuario if: "+tipoUsuario);
			} else {
				tipoUsuario = "NO";
			}
		} catch (Exception e) {
			System.out.println("Error general: "+e.getMessage());
			System.out.println("Debe ser qui error de conexion: "+e.getMessage());
		}
		
		return tipoUsuario;
	}

	@Override
	public int crearUsuario(String nombre, String apePat, String apeMat, String direccion, String password,
			String tipoUsuario, String telefono, String email, String cp, String ciudad) {
		String insertarUsuario = "insert usuario (nombre,apellidoPaterno,apellidoMaterno,direccion,password,tipoUsuario,telefono,email,codigoPostal,ciudad) values (?,?,?,?,?,?,?,?,?,?)";
		int afectados = 0;
		// String nuevoTipoUsuario = "";
		try {
			//con = conectardb.getConnection();
			con = conectardb.conectarDB();
			ps = con.prepareStatement(insertarUsuario);
			ps.setString(1, nombre);
			ps.setString(2, apePat);
			ps.setString(3, apeMat);
			ps.setString(4, direccion);
			ps.setString(5, password);
			ps.setString(6, tipoUsuario);
			ps.setString(7, telefono);
			ps.setString(8, email);
			ps.setString(9, cp);
			ps.setString(10, ciudad);
			afectados = ps.executeUpdate();
			// nuevoTipoUsuario = tipoUsuario;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			// mensaje = "FAIL";
			afectados = 0;
			// nuevoTipoUsuario = "0";
		}

		return afectados;
	}

	@Override
	public ArrayList<UsuarioM> verUsuario() {
		String consulta = "select * from usuario";
		ArrayList<UsuarioM> contenedorObj = new ArrayList<>();
		try {
			con = conectardb.conectarDB();
			ps = con.prepareStatement(consulta);
			rs = ps.executeQuery();

			while (rs.next()) {
				UsuarioM usuario = new UsuarioM();
				usuario.setIdUsuario(rs.getInt("idUsuario"));
				usuario.setNombre(rs.getString("nombre"));
				usuario.setApellidoPaterno(rs.getString("apellidoPaterno"));
				usuario.setApellidoMaterno(rs.getString("apellidoMaterno"));
				usuario.setDireccion(rs.getString("direccion"));
				usuario.setPassword(rs.getString("password"));
				usuario.setTipoUsuario(rs.getString("tipoUsuario"));
				usuario.setTelefono(rs.getString("telefono"));
				usuario.setEmail(rs.getString("email"));
				usuario.setCodigoPostal(rs.getString("codigoPostal"));
				usuario.setCiudad(rs.getString("ciudad"));

				contenedorObj.add(usuario);
			}
		} catch (Exception e) {
			System.out.println("Error al verUsuario: "+e.getMessage());
		}
		
		return contenedorObj;
	}

	@Override
	public int borrarUsuario(int idUsuario) {
		int afectados = 0;
		String borrarUsuario = "delete from usuario where idUsuario=?";

		try {
			///con = conectardb.getConnection();
			con = conectardb.conectarDB();
			ps = con.prepareStatement(borrarUsuario);
			ps.setInt(1, idUsuario);

			afectados = ps.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
			afectados = 0;
		}
		
		return afectados;
	}

	@Override
	public int actualizarUsuario(int idUsuario, String nombre, String apellidoPaterno, String apellidoMaterno, String direccion,
			String password, String tipoUsuario, String telefono, String email, String codigoPostal,String ciudad) {
		int respuesta = 0;
		String actualizarUsuario = "update usuario set nombre=?,apellidoPaterno=?,apellidoMaterno=?,direccion=?,password=?,tipoUsuario=?,telefono=?,email=?,codigoPostal=?,ciudad=? where idUsuario=?";
		
		try {
			//con = conectardb.getConnection();
			con = conectardb.conectarDB();
			ps = con.prepareStatement(actualizarUsuario);
			
			ps.setString(1, nombre);
			ps.setString(2, apellidoPaterno);
			ps.setString(3, apellidoMaterno);
			ps.setString(4, direccion);
			ps.setString(5, password);
			ps.setString(6, tipoUsuario);
			ps.setString(7, telefono);
			ps.setString(8, email);
			ps.setString(9, codigoPostal);
			ps.setString(10, ciudad);
			ps.setInt(11, idUsuario);
			
			respuesta = ps.executeUpdate();
		} catch (Exception e) {
			respuesta = 0;
		}
		
		return respuesta;
	}

}
