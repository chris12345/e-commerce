package com.develop.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.ArrayList;

import com.develop.config.ConectarDB;
import com.develop.interfaces.ProductoI;
import com.develop.models.ProductoM;

public class ProductoDAO implements ProductoI {
	ConectarDB conectardb = new ConectarDB();
	PreparedStatement ps;
	ResultSet rs;
	Connection conn;

	@Override
	public ArrayList<ProductoM> verProductos() {
		// TODO Auto-generated method stub
		String queryProducto = "select * from producto";
		// RequestDispatcher rd;
		ArrayList<ProductoM> contenedorProductos = new ArrayList<>();
		try {
			conn = conectardb.conectarDB();
			ps = conn.prepareStatement(queryProducto);
			rs = ps.executeQuery();
			while (rs.next()) {
				ProductoM producto = new ProductoM();
				producto.setIdProducto(rs.getInt("idProducto"));
				producto.setNombre(rs.getString("nombreProducto"));
				producto.setDescripcion(rs.getString("descripcionProducto"));
				producto.setPrecioUnitario(rs.getDouble("precioUnitario"));
				producto.setTipoDescuento(rs.getString("tipoDescuento"));
				producto.setTipoCategoria(rs.getString("tipoCategoria"));
				producto.setDisponibilidad(rs.getInt("disponibilidad"));
				producto.setMarca(rs.getString("marca"));
				producto.setFechaCreacion(rs.getDate("fechaCreacion"));
				
				contenedorProductos.add(producto);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		return contenedorProductos;
	}

	@Override
	public int insertarProducto(String nombreProducto, String descripcion, double precioUnitario, String tipoDescuento,
			String tipoCategoria, int disponibilidad, String marca) {
		String crearProducto = "insert producto(nombreProducto,descripcionProducto,precioUnitario,tipoDescuento,tipoCategoria,disponibilidad,marca, fechaCreacion) values(?,?,?,?,?,?,?,?)";
		int resultado = 0;
		//LocalDate fecha = LocalDate.now();
		
		try {
			conn = conectardb.conectarDB();
			ps = conn.prepareStatement(crearProducto);
			ps.setString(1, nombreProducto);
			ps.setString(2, descripcion);
			ps.setDouble(3, precioUnitario);
			ps.setString(4, tipoDescuento);
			ps.setString(5, tipoCategoria);
			ps.setInt(6, disponibilidad);
			ps.setString(7, marca);
			ps.setString(8,LocalDate.now().toString());
			
			resultado = ps.executeUpdate();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			resultado = 0;
		}
		return resultado;
	}

	@Override
	public int actualizarProducto(int idProducto, String nombre, String descripcion, double precioUnitario, String tipoDescuento, String tipoCategoria, int disponibilidad, String marca) {
		int respuesta = 0;
		String actualizarProducto = "update producto set nombreProducto=?"
				+ ", descripcionProducto=?, precioUnitario=?,tipoDescuento=?,tipoCategoria=?,disponibilidad=?,marca=? where idProducto=?";
		try {
			conn = conectardb.conectarDB();
			ps = conn.prepareStatement(actualizarProducto);
			
			ps.setString(1, nombre);
			ps.setString(2, descripcion);
			ps.setDouble(3, precioUnitario);
			ps.setString(4, tipoDescuento);
			ps.setString(5, tipoCategoria);
			ps.setInt(6, disponibilidad);
			ps.setString(7, marca);
			ps.setInt(8, idProducto);
			
			respuesta = ps.executeUpdate();
		} catch (Exception e) {
			respuesta = 0;
		}
		// TODO Auto-generated method stub
		return respuesta;
	}

	@Override
	public int borrarProducto(int idProducto) {
		int afectados = 0;
		String queryBorrar = "delete from producto where idProducto=?";
		
		try {
			conn = conectardb.conectarDB();
			ps = conn.prepareStatement(queryBorrar);
			ps.setInt(1, idProducto);
			
			afectados = ps.executeUpdate();
		} catch (Exception e) {
			afectados = 0;
		}
		
		return afectados;
	}

	@Override
	public ArrayList<ProductoM> filtrarProducto(String tipoCategoria) {
		//String filtro = (tipoCategoria != "") ? ;
		String consulta = "select * from producto where tipoCategoria='"+tipoCategoria+"'";
		//System.out.println("consulta: "+consulta);
		ArrayList<ProductoM> contenedor = new ArrayList<>(); 
		try {
			conn = conectardb.conectarDB();
			ps = conn.prepareStatement(consulta);
			rs = ps.executeQuery();
			
			while(rs.next()) {
				ProductoM producto = new ProductoM();
				
				producto.setNombre(rs.getString("nombreProducto"));
				producto.setDescripcion(rs.getString("descripcionProducto"));
				producto.setTipoDescuento(rs.getString("tipoDescuento"));
				producto.setPrecioUnitario(rs.getDouble("precioUnitario"));
				contenedor.add(producto);
			}
			
		} catch (Exception e) {
			e.getMessage();
		}
		
		return contenedor;
	}

}
