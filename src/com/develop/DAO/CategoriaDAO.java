package com.develop.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.develop.config.ConectarDB;
import com.develop.interfaces.CategoriaI;
import com.develop.models.CategoriaM;

public class CategoriaDAO implements CategoriaI{
	ConectarDB conectar = new ConectarDB();
	PreparedStatement ps;
	ResultSet rs;
	Connection con;
	
	@Override
	public ArrayList<CategoriaM> verCategorias() {
		ArrayList<CategoriaM> contenedorCategoria = new ArrayList<>(); 
		String filtrarCategoria = "select * from categoria";
		
		try {
			conectar = new ConectarDB();
			con = conectar.conectarDB();
			ps = con.prepareStatement(filtrarCategoria);
			rs = ps.executeQuery();
			
			while(rs.next()) {
				CategoriaM cat = new CategoriaM();
				
				cat.setIdCategoria(rs.getInt("idCategoria"));
				cat.setTipoCategoria(rs.getString("tipoCategoria"));
				cat.setDescripcionCategoria(rs.getString("descripcionCategoria"));
				
				contenedorCategoria.add(cat);
			}
			
		} catch (Exception e) {
			System.out.println("Debe ser qui error de conexion: "+e.getMessage());
		}
		return contenedorCategoria;
	}

	@Override
	public int borrarCategoria(int idCategoria) {
		String borrarRegistro = "delete from categoria where idCategoria=?";
		int respuesta = 0;
		
		try {
			con = conectar.conectarDB();
			ps = con.prepareStatement(borrarRegistro);
			
			ps.setInt(1, idCategoria);
			
			respuesta = ps.executeUpdate();
		} catch (Exception e) {
			respuesta = 0;
		}
		
		return respuesta;
	}

	@Override
	public int crearCategoria(String tipoCategoria, String descripcionCategoria) {
		String crearCategoria = "insert categoria (tipoCategoria,descripcionCategoria) values(?,?)";
		int resultado = 0;
		
		try {
			con=conectar.conectarDB();
			ps = con.prepareStatement(crearCategoria);
			
			ps.setString(1, tipoCategoria);
			ps.setString(2, descripcionCategoria);
			
			resultado = ps.executeUpdate();
		} catch (Exception e) {
			e.getMessage();
			resultado = 0;
		}
		return resultado;
	}

	@Override
	public int actualizarCategoria(int idCategoria, String tipoCategoria, String descripcionCategoria) {
		int respuesta =1;
		
		String actualizar = "update categoria set tipoCategoria=?, descripcionCategoria=? where idCategoria=?";
		
		try {
			con=conectar.conectarDB();
			ps = con.prepareStatement(actualizar);
			
			ps.setString(1, tipoCategoria);
			ps.setString(2, descripcionCategoria);
			ps.setInt(3, idCategoria);
			
			respuesta = ps.executeUpdate();
		} catch (Exception e) {
			
			System.out.println("excepcion");
			System.out.println(e.getMessage());
			respuesta=0;
		}
		return respuesta;
	}


}
