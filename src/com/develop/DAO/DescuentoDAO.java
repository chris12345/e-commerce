package com.develop.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.develop.config.ConectarDB;
import com.develop.interfaces.DescuentoI;
import com.develop.models.DescuentoM;

public class DescuentoDAO implements DescuentoI {

	ConectarDB conectarDb = new ConectarDB();
	PreparedStatement ps;
	ResultSet rs;
	Connection con;
	
	@Override
	public ArrayList<DescuentoM> verDescuento() {
		String consulta = "select * from descuento";
		ArrayList<DescuentoM> contenedorDescuento = new ArrayList<>();
		try {
			con = conectarDb.conectarDB();
			ps = con.prepareStatement(consulta);
			rs = ps.executeQuery();
			
			while(rs.next()) {
				DescuentoM descuentoM = new DescuentoM();
				descuentoM.setIdDescuento(rs.getInt("idDescuento"));
				descuentoM.setTipoDescuento(rs.getString("tipoDescuento"));
				descuentoM.setFechaInicio(rs.getDate("fechaInicio"));
				descuentoM.setFechaFin(rs.getDate("fechafin"));
				descuentoM.setDescuento(rs.getDouble("cantidadDescuento"));
				
				contenedorDescuento.add(descuentoM);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return contenedorDescuento;
	}

	@Override
	public int borrarDescuento(int idDescuento) {
		String borrarDescuento = "delete from descuento where idDescuento = ?";
		int respuesta = 0;
		try {
			con = conectarDb.conectarDB();
			ps = con.prepareStatement(borrarDescuento);
			
			ps.setInt(1, idDescuento);
			respuesta = ps.executeUpdate();
		} catch (Exception e) {
			respuesta = 0;
		}
		return respuesta;
	}

	@Override
	public int crearDescuento(String tipoDescuento,String fechaInicio, String fechafin, double cantidadDescuento) {
		String crearDescuento = "insert descuento (tipoDescuento, fechaInicio, fechafin, cantidadDescuento) values (?,?,?,?)";
		int respuesta=0;
		
		try {
			con = conectarDb.conectarDB();
			ps = con.prepareStatement(crearDescuento);
			ps.setString(1, tipoDescuento);
			ps.setString(2, fechaInicio);
			ps.setString(3, fechafin);
			ps.setDouble(4, cantidadDescuento);
			
			respuesta = ps.executeUpdate();
		} catch (Exception e) {
			e.getMessage();
			respuesta = 0;
		}
		
		return respuesta;
	}

	@Override
	public int actualizarDescuento(int idDescuento, String tipoDescuento, String fechaInicio, String fechafin,
			double cantidadDescuento) {
		String actualizarDescuento = "update descuento set tipoDescuento=?, fechaInicio=?, fechafin=?, cantidadDescuento=? where idDescuento=?";
		int resultado = 0;
		try {
			con = conectarDb.conectarDB();
			ps = con.prepareStatement(actualizarDescuento);
			
			ps.setString(1, tipoDescuento);
			ps.setString(2, fechaInicio);
			ps.setString(3, fechafin);
			ps.setDouble(4, cantidadDescuento);
			ps.setInt(5, idDescuento);
			
			resultado = ps.executeUpdate();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			resultado = 0;
		}
		
		return resultado;
	}
	
}
