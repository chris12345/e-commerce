package com.develop.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.develop.config.ConectarDB;
import com.develop.interfaces.PedidoI;
import com.develop.models.PedidoM;
import com.itextpdf.text.log.SysoCounter;

public class PedidoDAO implements PedidoI {
	ConectarDB conectarDb = new ConectarDB();
	PreparedStatement ps;
	ResultSet rs;
	Connection con;

	@Override
	public ArrayList<PedidoM> verPedido() {
		String consulta = "select * from pedido";

		ArrayList<PedidoM> almacenObj = new ArrayList<>();
		try {
			con = conectarDb.conectarDB();
			ps = con.prepareStatement(consulta);
			rs = ps.executeQuery();

			while (rs.next()) {
				PedidoM pedidoM = new PedidoM();
				pedidoM.setIdPedido(rs.getInt("idPedido"));
				pedidoM.setReferenciaPedido(rs.getString("referenciaPedido"));
				pedidoM.setNombreProducto(rs.getString("nombreProducto"));
				pedidoM.setPrecioUnitario(rs.getDouble("precioUnitario"));
				pedidoM.setCantidad(rs.getInt("cantidad"));
				pedidoM.setNombreUsuario(rs.getString("nombreUsuario"));
				pedidoM.setFechaCreacion(rs.getDate("fechaCreacion"));
				pedidoM.setFechaExpiracion(rs.getDate("fechaExpiracion"));
				pedidoM.setStatusCompra(rs.getString("statusCompra"));
				pedidoM.setStatusPedido(rs.getString("statusPedido"));

				almacenObj.add(pedidoM);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return almacenObj;
	}

	@Override
	public int crearPedido(String referencia, String nombreProducto, double precioUnitario, int cantidad,String nombreUsuario,
			String fechaCreacion, String fechaExpiracion,String statusCompra, String statusPedido) {

		String crearPedido = "insert pedido(referenciaPedido,nombreProducto,precioUnitario,cantidad,nombreUsuario,fechaCreacion,"
				+ "fechaExpiracion,statusCompra,statusPedido) values (?,?,?,?,?,?,?,?,?)";
		
		int respuesta = 0;
		
		try {
			con = conectarDb.conectarDB();
			ps = con.prepareStatement(crearPedido);

			ps.setString(1, referencia);
			ps.setString(2, nombreProducto);
			ps.setDouble(3, precioUnitario);
			ps.setInt(4, cantidad);
			ps.setString(5, nombreUsuario);
			ps.setString(6, fechaCreacion);
			ps.setString(7, fechaExpiracion);
			ps.setString(8, statusCompra);
			ps.setString(9, statusPedido);

			respuesta = ps.executeUpdate();
		} catch (Exception e) {
			respuesta = 0;
			System.out.println(e.getMessage());
		}

		return respuesta;
	}

	@Override
	public int borrarPedido(int idPedido) {
		String borrarPedido = "delete from pedido where idPedido=?";
		int resultado = 0;

		try {
			con = conectarDb.conectarDB();
			ps = con.prepareStatement(borrarPedido);

			ps.setInt(1, idPedido);
			resultado = ps.executeUpdate();
		} catch (Exception e) {
			resultado = 0;
			System.out.println(e.getMessage());
		}

		return resultado;
	}

	@Override
	public int actualizarPedido(int idPedido, String referencia, String nombreProducto, double precioUnitario, 
			int cantidad,String nombreUsuario, String fechaCreacion, String fechaExpiracion,String statusCompra, String statusPedido) {
		String actualizarPedido = "update pedido set referenciaPedido=?,nombreProducto=?,precioUnitario=?"
				+ ",cantidad=?, nombreUsuario=?, fechaCreacion=?, fechaExpiracion=?,statusCompra=?,statusPedido=? where idPedido=?";
		int respuesta = 0;
		try {
			con = conectarDb.conectarDB();
			ps = con.prepareStatement(actualizarPedido);

			ps.setString(1, referencia);
			ps.setString(2, nombreProducto);
			ps.setDouble(3, precioUnitario);
			ps.setInt(4, cantidad);
			ps.setString(5, nombreUsuario);
			ps.setString(6, fechaCreacion);
			ps.setString(7, fechaExpiracion);
			ps.setString(8, statusCompra);
			ps.setString(9, statusPedido);
			ps.setInt(10, idPedido);

			respuesta = ps.executeUpdate();
		} catch (Exception e) {
			respuesta = 0;
			System.out.println(e.getMessage());
		}
		return respuesta;
	}

	@Override
	public ArrayList<PedidoM> filtrarPedido(String nombreUsuario) {
		String consulta = "select * from pedido where nombreUsuario='"+nombreUsuario+"' and statusCompra='Creado'";
		ArrayList<PedidoM> almacenPedido = new ArrayList<>();
		try {
			con=conectarDb.conectarDB();
			ps = con.prepareStatement(consulta);
			rs=ps.executeQuery();
			
			while(rs.next()) {
				PedidoM pedidoM = new PedidoM();
				
				pedidoM.setIdPedido(rs.getInt("idPedido"));
				pedidoM.setNombreProducto(rs.getString("nombreProducto"));
				pedidoM.setPrecioUnitario(rs.getDouble("precioUnitario"));
				pedidoM.setCantidad(rs.getInt("cantidad"));
				
				almacenPedido.add(pedidoM);
			}
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		return almacenPedido;
	}

	@Override
	public int actualizarPedidoCarrito(String nombreUsuario) {
		String actualizarPedido = "update pedido set statusCompra='Pagado' where nombreUsuario='"+nombreUsuario+"'";
		int resultado = 0;
		try {
			con = conectarDb.conectarDB();
			ps = con.prepareStatement(actualizarPedido);
			
			resultado=ps.executeUpdate();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			resultado = 0;
		}
		return resultado;
	}

	@Override
	public int cancelarPedidoCarrito(String nombreUsuario) {
		String cancelarPedidoCarrito = "delete from pedido where nombreUsuario='"+nombreUsuario+"'";
		int resultado = 0;
		try {
			con = conectarDb.conectarDB();
			ps = con.prepareStatement(cancelarPedidoCarrito);
			
			resultado=ps.executeUpdate();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			resultado=0;
		}
		return resultado;
	}

	@Override
	public ArrayList<PedidoM> filtroParaEncargado() {
		String consulta = "select referenciaPedido, statusPedido from pedido where statusCompra='Pagado' and statusPedido='Pendiente' GROUP BY referenciaPedido HAVING COUNT(*)>0";
		ArrayList<PedidoM> almacenPedido = new ArrayList<>();
		
		try {
			con = conectarDb.conectarDB();
			ps = con.prepareStatement(consulta);
			rs = ps.executeQuery();
			while(rs.next()) {
				PedidoM pedido = new PedidoM();
				pedido.setReferenciaPedido(rs.getString("referenciaPedido"));
				pedido.setStatusPedido(rs.getString("statusPedido"));
				almacenPedido.add(pedido);
			}
		} catch (Exception e) {
			System.out.println("Error para ver pedidos: "+e.getMessage());
		}
		return almacenPedido;
	}

	@Override
	public ArrayList<PedidoM> filtroProductoEncargado(String referenciaPedido) {
		String consulta = "select nombreProducto,precioUnitario,cantidad,statusPedido  from pedido where referenciaPedido='"+referenciaPedido+"'";
		ArrayList<PedidoM> contenedorPedido = new ArrayList<>();
		try {
			con=conectarDb.conectarDB();
			ps = con.prepareStatement(consulta);
			rs = ps.executeQuery();
			
			while(rs.next()) {
				PedidoM pedidoM = new PedidoM();
				pedidoM.setNombreProducto(rs.getString("nombreProducto"));
				pedidoM.setPrecioUnitario(rs.getDouble("precioUnitario"));
				pedidoM.setCantidad(rs.getInt("cantidad"));
				pedidoM.setStatusPedido(rs.getString("statusPedido"));
				
				contenedorPedido.add(pedidoM);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return contenedorPedido;
	}

	@Override
	public int actualizarPedidoEncargado(String referencia, String statusPedido) {
		String actualizar = "update pedido set statusPedido=? where referenciaPedido = ?";
		int resultado = 0;
		try {
			con = conectarDb.conectarDB();
			ps = con.prepareStatement(actualizar);
			
			ps.setString(1, statusPedido);
			ps.setString(2, referencia);
			
			resultado = ps.executeUpdate();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			resultado = 0;
		}
		return resultado;
	}

}
