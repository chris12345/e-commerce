package com.develop.models;

import java.util.Date;

public class ProductoM {
	private int idProducto;
	private String nombre;
	private String descripcion;
	private double precioUnitario;
	private String tipoDescuento;
	private String tipoCategoria;
	private int disponibilidad;
	private String marca;
	private Date fechaCreacion;
	
	public ProductoM() {
		
	}

	protected ProductoM(int idProducto, String nombre, String descripcion, double precioUnitario, String tipoDescuento,
			String tipoCategoria, int disponibilidad, String marca,Date fechaCreacion) {
		super();
		this.idProducto = idProducto;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.precioUnitario = precioUnitario;
		this.tipoDescuento = tipoDescuento;
		this.tipoCategoria = tipoCategoria;
		this.disponibilidad = disponibilidad;
		this.marca = marca;
		this.fechaCreacion = fechaCreacion;
	}

	public int getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public double getPrecioUnitario() {
		return precioUnitario;
	}

	public void setPrecioUnitario(double precioUnitario) {
		this.precioUnitario = precioUnitario;
	}

	public String getTipoDescuento() {
		return tipoDescuento;
	}

	public void setTipoDescuento(String tipoDescuento) {
		this.tipoDescuento = tipoDescuento;
	}

	public String getTipoCategoria() {
		return tipoCategoria;
	}

	public void setTipoCategoria(String tipoCategoria) {
		this.tipoCategoria = tipoCategoria;
	}

	public int getDisponibilidad() {
		return disponibilidad;
	}

	public void setDisponibilidad(int disponibilidad) {
		this.disponibilidad = disponibilidad;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}
	
	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@Override
	public String toString() {
		return "ProductoM [idProducto=" + idProducto + ", nombre=" + nombre + ", descripcion=" + descripcion
				+ ", precioUnitario=" + precioUnitario + ", tipoDescuento=" + tipoDescuento + ", tipoCategoria="
				+ tipoCategoria + ", disponibilidad=" + disponibilidad + ", marca=" + marca + ", fechaCreacion="+fechaCreacion+ "]";
	}
}
