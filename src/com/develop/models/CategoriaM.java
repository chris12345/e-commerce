package com.develop.models;

public class CategoriaM {
	private int idCategoria;
	private String tipoCategoria;
	private String descripcionCategoria;
	
	public CategoriaM() {
		
	}

	public CategoriaM(int idCategoria, String tipoCategoria, String descripcionCategoria) {
		super();
		this.idCategoria = idCategoria;
		this.tipoCategoria = tipoCategoria;
		this.descripcionCategoria = descripcionCategoria;
	}

	public int getIdCategoria() {
		return idCategoria;
	}

	public void setIdCategoria(int idCategoria) {
		this.idCategoria = idCategoria;
	}

	public String getTipoCategoria() {
		return tipoCategoria;
	}

	public void setTipoCategoria(String tipoCategoria) {
		this.tipoCategoria = tipoCategoria;
	}

	public String getDescripcionCategoria() {
		return descripcionCategoria;
	}

	public void setDescripcionCategoria(String descripcionCategoria) {
		this.descripcionCategoria = descripcionCategoria;
	}

	@Override
	public String toString() {
		return "CategoriaM [idCategoria=" + idCategoria + ", tipoCategoria=" + tipoCategoria + ", descripcionCategoria="
				+ descripcionCategoria + "]";
	}
}
