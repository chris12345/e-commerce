package com.develop.models;

import java.util.Date;

public class DescuentoM {
	private int idDescuento;
	private String tipoDescuento;
	private Date fechaInicio;
	private Date fechaFin;
	private double descuento;
	
	public DescuentoM() {
		
	}

	public DescuentoM(int idDescuento, String tipoDescuento, Date fechaInicio, Date fechaFin, double descuento) {
		super();
		this.idDescuento = idDescuento;
		this.tipoDescuento = tipoDescuento;
		this.fechaInicio = fechaInicio;
		this.fechaFin = fechaFin;
		this.descuento = descuento;
	}

	public int getIdDescuento() {
		return idDescuento;
	}

	public void setIdDescuento(int idDescuento) {
		this.idDescuento = idDescuento;
	}

	public String getTipoDescuento() {
		return tipoDescuento;
	}

	public void setTipoDescuento(String tipoDescuento) {
		this.tipoDescuento = tipoDescuento;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public double getDescuento() {
		return descuento;
	}

	public void setDescuento(double descuento) {
		this.descuento = descuento;
	}

	@Override
	public String toString() {
		return "DescuentoM [idDescuento=" + idDescuento + ", tipoDescuento=" + tipoDescuento + ", fechaInicio="
				+ fechaInicio + ", fechaFin=" + fechaFin + ", descuento=" + descuento + "]";
	}
}
