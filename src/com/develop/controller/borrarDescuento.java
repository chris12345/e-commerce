package com.develop.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.DescuentoDAO;

/**
 * Servlet implementation class borrarDescuento
 */
@WebServlet("/borrarDescuento")
public class borrarDescuento extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public borrarDescuento() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		RequestDispatcher rd;
		String idDescuento = request.getParameter("idDescuento");
		DescuentoDAO descuentoDAO = new DescuentoDAO();
		int respuesta = descuentoDAO.borrarDescuento(Integer.parseInt(idDescuento));
		
		if(respuesta>=1) {
			System.out.println("Se borro men xd");
			rd = request.getRequestDispatcher("/vistas/descuento/descuento.jsp");
			rd.forward(request, response);
		}else {
			System.out.println("fallo el borrado O.o");
			rd = request.getRequestDispatcher("/vistas/descuento/descuento.jsp");
			rd.forward(request, response);
		}
		
	}

}
