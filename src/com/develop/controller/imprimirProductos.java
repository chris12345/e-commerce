package com.develop.controller;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.ProductoDAO;
import com.develop.models.ProductoM;

/**
 * Servlet implementation class imprimirProductos
 */
@WebServlet("/imprimirProductos")
public class imprimirProductos extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public imprimirProductos() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher rd;
		
		File carpeta = new File("C:/Users/Khryz/Documents/workspace-spring-tool-suite-4-4.11.0.RELEASE/E-comerce/Productos");
		File archivo = new File(carpeta.getPath() + "/Productos.txt");

		try {
			ProductoDAO producto = new ProductoDAO();
			ArrayList<ProductoM> almacenProducto = producto.verProductos();
			//RequestDispatcher rd;

			boolean banderaCarpeta = carpeta.mkdir();
			boolean banderaArchivo = archivo.createNewFile();

			Scanner sc = new Scanner(archivo);

			FileWriter fileWriter = new FileWriter(archivo);

			// System.out.println("Carpeta: "+banderaCarpeta);
			// System.out.println("Archivo: "+banderaArchivo);

			if (almacenProducto != null) {
				System.out.println("Hay contenido en la base de datos");
				if (banderaCarpeta && banderaArchivo) {
					System.out.println("Carpeta: " + banderaCarpeta);
					System.out.println("Archivo: " + banderaArchivo);

					for (ProductoM productos : almacenProducto) {
						fileWriter.write(productos.toString() + "\n");
					}
					fileWriter.close();
					rd = request.getRequestDispatcher("/vistas/productos/productos.jsp");
					rd.forward(request, response);
				} else {
					System.out.println("No se crearon men");
					if (sc.hasNext()) {
						System.out.println("Hay contenido en el .txt");
						while (sc.hasNext()) {
							System.out.println(sc.nextLine());
						}
						rd = request.getRequestDispatcher("/vistas/productos/productos.jsp");
						rd.forward(request, response);
					} else {
						for (ProductoM productos : almacenProducto) {
							fileWriter.write(productos.toString() + "\n");
						}
						fileWriter.close();
						rd = request.getRequestDispatcher("/vistas/productos/productos.jsp");
						rd.forward(request, response);
					}
				}
			} else {
				System.out.println("La base de datos esta vacia");
			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			rd = request.getRequestDispatcher("/vistas/productos/productos.jsp");
			rd.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// doGet(request, response);
		// System.out.println("Hola dopost");
	}

}
