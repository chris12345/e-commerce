package com.develop.controller;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.ProductoDAO;
import com.develop.DAO.UsuarioDAO;
import com.develop.models.ProductoM;
import com.develop.models.UsuarioM;
import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * Servlet implementation class imprimirProductosPDF
 */
@WebServlet("/imprimirProductosPDF")
public class imprimirProductosPDF extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public imprimirProductosPDF() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// response.getWriter().append("Served at: ").append(request.getContextPath());
		// System.out.println("doget");//*********************
		Document documento = new Document();
		RequestDispatcher rd;
		ProductoDAO productoDao = new ProductoDAO();
		// Se crea el OutputStream para el fichero donde queremos dejar el pdf.
		FileOutputStream ficheroPdf;
		try {
			ficheroPdf = new FileOutputStream(
					"C:/Users/Khryz/Documents/workspace-spring-tool-suite-4-4.11.0.RELEASE/E-comerce/Reportes/PDF/Productos.pdf");

			ArrayList<ProductoM> contenedor = productoDao.verProductos();

			// Se asocia el documento al OutputStream y se indica que el espaciado entre
			// lineas sera de 20. Esta llamada debe hacerse antes de abrir el documento
			PdfWriter.getInstance(documento, ficheroPdf).setInitialLeading(20);

			// Se abre el documento.
			documento.open();
			documento.addTitle("Lista de Productos");
			// documento.add(new Paragraph("Esto es el primer p�rrafo, normalito"));

			if (contenedor != null) {
				for (ProductoM productos : contenedor) {
					documento.add(new Paragraph(productos.toString() + "\n"));
				}
			} else {
				System.out.println("No hay nada en la tabla");
			}

			documento.close();
			System.out.println("Se creo");
			
			rd = request.getRequestDispatcher("/vistas/productos/productos.jsp");
			rd.forward(request, response);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			rd = request.getRequestDispatcher("/vistas/productos/productos.jsp");
			rd.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// doGet(request, response);
		System.out.println("dopost");
	}

}
