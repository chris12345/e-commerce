package com.develop.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.CategoriaDAO;

/**
 * Servlet implementation class crearCategoria
 */
@WebServlet("/crearCategoria")
public class crearCategoria extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public crearCategoria() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		RequestDispatcher rd;
		int respuesta;
		
		CategoriaDAO categoriaDao = new CategoriaDAO();
		
		String tipoCategoria = request.getParameter("tipoCategoria");
		String descripcionCategoria = request.getParameter("descripcionCategoria");
		
		System.out.println(tipoCategoria);
		System.out.println(descripcionCategoria);
		
		respuesta = categoriaDao.crearCategoria(tipoCategoria, descripcionCategoria);
		
		if(respuesta>=1) {
			System.out.println("Se creo la categoria");
			rd = request.getRequestDispatcher("/vistas/categoria/categoria.jsp");
			rd.forward(request, response);
		}else {
			System.out.println("Se creo la categoria");
			rd = request.getRequestDispatcher("/vistas/categoria/categoria.jsp");
			rd.forward(request, response);
		}
	}

}
