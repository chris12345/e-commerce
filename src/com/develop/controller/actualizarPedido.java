package com.develop.controller;

import java.io.IOException;
import java.time.LocalDate;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.PedidoDAO;

/**
 * Servlet implementation class actualizarPedido
 */
@WebServlet("/actualizarPedido")
public class actualizarPedido extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public actualizarPedido() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("Hola doget pedido");
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//System.out.println("Hola doget pedido");
		RequestDispatcher rd;
		
		String idPedido = request.getParameter("idPedido");
		String referenciaPedido = request.getParameter("referenciaPedido");
		String nombreProducto = request.getParameter("nombreProducto");
		String precioUnitario = request.getParameter("precioUnitario");
		String cantidad = request.getParameter("cantidad");
		
		String nombreUsuario = request.getParameter("nombreUsuario");
		String fechaCreacion = request.getParameter("fechaCreacion");
		String fechaExpiracion = request.getParameter("fechaExpiracion");
		String statusCompra = request.getParameter("statusCompra");
		String statusPedido = request.getParameter("statusPedido");
		
		LocalDate nuevaFechaCreacion = LocalDate.parse(fechaCreacion);
		LocalDate nuevaFechaExpiracion = LocalDate.parse(fechaExpiracion);
		
		PedidoDAO pedidoDao = new PedidoDAO();
		
		int respuesta = pedidoDao.actualizarPedido(Integer.parseInt(idPedido), referenciaPedido, nombreProducto, Double.parseDouble(precioUnitario),
				Integer.parseInt(cantidad), nombreUsuario,nuevaFechaCreacion.toString(),nuevaFechaExpiracion.toString(),statusCompra,statusPedido);
		 
		if(respuesta >= 0) {
			System.out.println("Se actualiz� el pedido xd");
			rd = request.getRequestDispatcher("/vistas/pedido/pedido.jsp");
			rd.forward(request, response);
		}else {
			System.out.println("No se actualiz� el pedido xc");
			rd = request.getRequestDispatcher("/vistas/pedido/pedido.jsp");
			rd.forward(request, response);
		}
	}

}
