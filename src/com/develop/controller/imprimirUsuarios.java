package com.develop.controller;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.UsuarioDAO;
import com.develop.models.ProductoM;
import com.develop.models.UsuarioM;

/**
 * Servlet implementation class imprimirUsuarios
 */
@WebServlet("/imprimirUsuarios")
public class imprimirUsuarios extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public imprimirUsuarios() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher rd;

		File carpeta = new File("C:/Users/Khryz/Documents/workspace-spring-tool-suite-4-4.11.0.RELEASE/E-comerce/Usuarios");
		File archivo = new File(carpeta.getPath() + "/Usuarios.txt");

		UsuarioDAO usuarioDao = new UsuarioDAO();
		ArrayList<UsuarioM> contenedorUsuario = usuarioDao.verUsuario();
		
		boolean banderaCarpeta = carpeta.mkdir();
		boolean banderaArchivo = archivo.createNewFile();

		Scanner sc = new Scanner(archivo);

		FileWriter fileWriter = new FileWriter(archivo);
		
		if (contenedorUsuario != null) {
			System.out.println("Hay contenido en la base de datos");
			if (banderaCarpeta && banderaArchivo) {
				System.out.println("Carpeta: " + banderaCarpeta);
				System.out.println("Archivo: " + banderaArchivo);

				for (UsuarioM usuarios : contenedorUsuario) {
					fileWriter.write(usuarios.toString() + "\n");
				}
				fileWriter.close();
				rd = request.getRequestDispatcher("/vistas/usuario/usuario.jsp");
				rd.forward(request, response);
			} else {
				System.out.println("No se crearon men");
				if (sc.hasNext()) {
					System.out.println("Hay contenido en el .txt");
					while (sc.hasNext()) {
						System.out.println(sc.nextLine());
					}
					rd = request.getRequestDispatcher("/vistas/usuario/usuario.jsp");
					rd.forward(request, response);
				} else {
					for (UsuarioM usuarios : contenedorUsuario) {
						fileWriter.write(usuarios.toString() + "\n");
					}
					fileWriter.close();
					rd = request.getRequestDispatcher("/vistas/usuario/usuario.jsp");
					rd.forward(request, response);
				}
			}
		} else {
			System.out.println("La base de datos esta vacia");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// doGet(request, response);
		// System.out.println("dopost");
	}

}
