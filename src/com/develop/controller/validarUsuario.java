package com.develop.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.UsuarioDAO;

/**
 * Servlet implementation class validarUsuario
 */
@WebServlet("/validarUsuario")
public class validarUsuario extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public validarUsuario() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// response.getWriter().append("Served at: ").append(request.getContextPath());
		// //Respuesta
		// System.out.println("Hola doGet");

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher rd;
		
		try {
			//RequestDispatcher rd;
			String nombre = request.getParameter("usuario");
			String password = request.getParameter("password");

			UsuarioDAO usuariodao = new UsuarioDAO();

			String respuesta = usuariodao.validarUsuario(nombre, password);
			
			request.getSession().setAttribute("usuarioSession", nombre);
			request.getSession().setAttribute("tipoSession", respuesta);
			
			if (respuesta.equals("Administrador")) {
				System.out.println("Redireccionando Administrador");

				rd = request.getRequestDispatcher("/verCategoria?tipoUsuario="+respuesta);
				rd.forward(request, response);
				
			} else if (respuesta.equals("Cliente")) {
				System.out.println("Redireccionando Cliente");

				rd = request.getRequestDispatcher("/verCategoria?tipoUsuario="+respuesta);
				rd.forward(request, response);
			} else if(respuesta.equals("Encargado")) {
				System.out.println("Redireccionando Encargado");

				rd = request.getRequestDispatcher("/vistas/main/encargado/encargado.jsp");
				rd.forward(request, response);
			} else {
				System.out.println("no");
				rd = request.getRequestDispatcher("/login/login.jsp");
				rd.forward(request, response);
			}
		/*}catch(ClassNotFoundException cls) {
			System.out.println("Class not found: "+cls.getMessage());
			rd = request.getRequestDispatcher("/login/Error/error.jsp");
			rd.forward(request, response);*/
		}catch(SQLException sql) {
			System.out.println("MySQLException: "+sql.getMessage());
			rd = request.getRequestDispatcher("/login/Error/error.jsp");
			rd.forward(request, response);
		}catch (Exception e) {
			System.out.println("Alguna otra excepcion: "+e.getMessage());
			rd = request.getRequestDispatcher("/login/Error/error.jsp");
			rd.forward(request, response);
		}
		
	}

}
