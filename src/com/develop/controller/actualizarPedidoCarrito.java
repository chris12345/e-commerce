package com.develop.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.PedidoDAO;

/**
 * Servlet implementation class actualizarPedidoCarrito
 */
@WebServlet("/actualizarPedidoCarrito")
public class actualizarPedidoCarrito extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public actualizarPedidoCarrito() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		RequestDispatcher rd;
		PedidoDAO pedidoDao = new PedidoDAO();
		String nombreUsuario = request.getParameter("nombreUsuario");
		int respuesta = pedidoDao.actualizarPedidoCarrito(nombreUsuario);
		
		if(respuesta>=1) {
			System.out.println("Si se pago el carrito xd");
			rd = request.getRequestDispatcher("/vistas/main/main.jsp");
			rd.forward(request, response);
		}else {
			System.out.println("No se pago el carrito xc");
			rd = request.getRequestDispatcher("/vistas/main/main.jsp");
			rd.forward(request, response);
		}
	}

}
