package com.develop.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.CategoriaDAO;
import com.develop.models.CategoriaM;

/**
 * Servlet implementation class verCategoria
 */
@WebServlet("/verCategoria")
public class verCategoria extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public verCategoria() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		//System.out.println("Hola doget");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		//System.out.println("Hola dopost");
		RequestDispatcher rd;
		CategoriaDAO categoria = new CategoriaDAO();
		
		String tipoUsuario = request.getParameter("tipoUsuario");
		System.out.println(tipoUsuario);
		
		ArrayList<CategoriaM> contenedor = categoria.verCategorias();
		
		if(contenedor!=null) {
			System.out.println("Hay contenido en categoria");
			
			if(tipoUsuario.equals("Administrador") ){
				rd = request.getRequestDispatcher("/vistas/main/main.jsp?tipoCategoria=Jamon&tipoUsuario="+tipoUsuario);
				rd.forward(request, response);
				
			}else if(tipoUsuario.equals("Cliente")){
				rd = request.getRequestDispatcher("/vistas/main/Cliente/main.jsp?tipoCategoria=Jamon&tipoUsuario="+tipoUsuario);
				rd.forward(request, response);
				
			}else if(tipoUsuario == "Encargado") {
				
			}
			
		}else {
			System.out.println("No hay nada en categoria");
			rd = request.getRequestDispatcher("/vistas/main/main.jsp?tipoCategoria=Jamon");
			rd.forward(request, response);
		}
	}

}
