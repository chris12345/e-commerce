package com.develop.controller;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.UsuarioDAO;
import com.develop.models.UsuarioM;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;

/**
 * Servlet implementation class listaUsuarioPDF
 */
@WebServlet("/listaUsuarioPDF")
public class listaUsuarioPDF extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final String RESULT = "Ejemplo1.<strong>pdf</strong>";
	public static final String CHUNK = "Titulo";
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public listaUsuarioPDF() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Document documento = new Document();
		RequestDispatcher rd;
		UsuarioDAO usuarioDao = new UsuarioDAO();
		// Se crea el OutputStream para el fichero donde queremos dejar el pdf.
		FileOutputStream ficheroPdf;
		try {
			ficheroPdf = new FileOutputStream("C:/Users/Khryz/Documents/workspace-spring-tool-suite-4-4.11.0.RELEASE/E-comerce/Reportes/PDF/Usuarios.pdf");
			
			ArrayList<UsuarioM> contenedor = usuarioDao.verUsuario();
			
			// Se asocia el documento al OutputStream y se indica que el espaciado entre
			// lineas sera de 20. Esta llamada debe hacerse antes de abrir el documento
			PdfWriter.getInstance(documento,ficheroPdf).setInitialLeading(20);

			// Se abre el documento.
			documento.open();
			documento.addTitle("Lista de Usuarios");
			//documento.add(new Paragraph("Esto es el primer p�rrafo, normalito"));
			
			if(contenedor!=null) {
				for(UsuarioM usuarios:contenedor) {
					documento.add(new Paragraph(usuarios.toString()+"\n"));
				}
			}else {
				System.out.println("No hay nada en la tabla");
			}
			
			documento.close();
			System.out.println("Se creo");
			rd = request.getRequestDispatcher("/vistas/usuario/usuario.jsp");
			rd.forward(request, response);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			rd = request.getRequestDispatcher("/vistas/usuario/usuario.jsp");
			rd.forward(request, response);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			rd = request.getRequestDispatcher("/vistas/usuario/usuario.jsp");
			rd.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// doGet(request, response);
		System.out.println("post");
	}

}
