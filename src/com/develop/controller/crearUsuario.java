package com.develop.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.UsuarioDAO;

/**
 * Servlet implementation class crearUsuario
 */
@WebServlet("/crearUsuario")
public class crearUsuario extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public crearUsuario() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		System.out.println("Hola doget");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//System.out.println("Hola dopost");
		//doGet(request, response);
		RequestDispatcher rd;
		UsuarioDAO usuarioDao = new UsuarioDAO();
		
		String nombre = request.getParameter("nombre");
		String apellidoPaterno = request.getParameter("apellidoPaterno");
		String apellidoMaterno = request.getParameter("apellidoMaterno");
		String direccion = request.getParameter("direccion");
		String password = request.getParameter("password");
		String tipoUsuario = request.getParameter("tipoUsuario");
		String telefono = request.getParameter("telefono");
		String email = request.getParameter("email");
		String cp = request.getParameter("cp");
		String ciudad = request.getParameter("ciudad");
		
		int respuesta = usuarioDao.crearUsuario(nombre,apellidoPaterno,apellidoMaterno,direccion,password,tipoUsuario,telefono,email,cp,ciudad);
		
		if(respuesta >= 1) {
			System.out.println("Se creo usuario");
			rd = request.getRequestDispatcher("/vistas/usuario/usuario.jsp");
			rd.forward(request, response);
		}else {
			System.out.println("Hubo un error al crear");
			rd = request.getRequestDispatcher("/vistas/usuario/usuario.jsp");
			rd.forward(request, response);
		}
		//System.out.println(tipoUsuario);
	}

}
