package com.develop.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.UsuarioDAO;

/**
 * Servlet implementation class actualizarUsuario
 */
@WebServlet("/actualizarUsuario")
public class actualizarUsuario extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public actualizarUsuario() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		System.out.println("Hola doget");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher rd;
		int respuesta = 0;
		
		
		UsuarioDAO usuarioDao = new UsuarioDAO();
		
		String idUsuario = request.getParameter("idUsuario");
		String nombreUsuario = request.getParameter("nombre");
		String apellidoPaterno = request.getParameter("apellidoPaterno");
		String apellidoMaterno = request.getParameter("apellidoMaterno");
		String direccion = request.getParameter("direccion");
		String password = request.getParameter("password");
		String tipoUsuario = request.getParameter("tipoUsuario");
		String telefono = request.getParameter("telefono");
		String email = request.getParameter("email");
		String cp = request.getParameter("cp");
		String ciudad = request.getParameter("ciudad");
		
		
		/*System.out.println("------------------------------------------");
		System.out.println(idUsuario);
		System.out.println(nombreUsuario);
		System.out.println(apellidoPaterno);
		System.out.println(apellidoMaterno);
		System.out.println(direccion);
		System.out.println(password);
		System.out.println(tipoUsuario);
		System.out.println(telefono);
		System.out.println(email);
		System.out.println(cp);
		System.out.println(ciudad);
		System.out.println("Hola dopost");*/
		
		respuesta = usuarioDao.actualizarUsuario(Integer.parseInt(idUsuario),nombreUsuario, apellidoPaterno, apellidoMaterno, direccion, password, tipoUsuario, telefono, email, cp, ciudad);
		
		System.out.println("Respuesta: "+respuesta);
		if(respuesta >= 1) {
			System.out.println("Se actualizo el usuario");
			rd = request.getRequestDispatcher("/vistas/usuario/usuario.jsp");
			rd.forward(request, response);
		}else {
			System.out.println("NO se actualizo el usuario");
			rd = request.getRequestDispatcher("/vistas/usuario/usuario.jsp");
			rd.forward(request, response);
		}
		
	}

}
