package com.develop.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.ProductoDAO;
import com.develop.models.ProductoM;

/**
 * Servlet implementation class filtrarProductos
 */
@WebServlet("/filtrarProductos")
public class filtrarProductos extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public filtrarProductos() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		RequestDispatcher rd;
		ProductoDAO productoDao = new ProductoDAO();
		
		String filtro = request.getParameter("tipoCategoria");
		List<ProductoM> productoM = productoDao.filtrarProducto(filtro);
		
		String tipoUsuario = request.getParameter("tipoUsuario");
		
		//System.out.println(tipoUsuario);
		request.setAttribute("tipoCategoria", filtro);
		if(productoM!=null) {
			//System.out.println("Si hay de esa categoria xd: "+filtro);
			if(tipoUsuario.equals("Administrador")) {
				rd = request.getRequestDispatcher("/vistas/main/main.jsp?tipoCategoria="+filtro);
				rd.forward(request, response);
				
			}else if(tipoUsuario.equals("Cliente")) {
				rd = request.getRequestDispatcher("/vistas/main/Cliente/main.jsp?tipoCategoria="+filtro);
				rd.forward(request, response);
			}else if(tipoUsuario.equals("Encargado")) {
				
			}
			
		}else {
			//System.out.println("No hay de esa categoria xc: "+filtro);
			rd = request.getRequestDispatcher("/vistas/main/main.jsp");
			rd.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		
		
	}

}
