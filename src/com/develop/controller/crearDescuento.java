package com.develop.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.DescuentoDAO;

/**
 * Servlet implementation class crearDescuento
 */
@WebServlet("/crearDescuento")
public class crearDescuento extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public crearDescuento() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		
		DescuentoDAO descuentoDao = new DescuentoDAO();
		RequestDispatcher rd;
		
		String tipoDescuento = request.getParameter("tipoDescuento");
		String fechaInicio = request.getParameter("fechaInicio");
		String fechafin = request.getParameter("fechafin");
		String cantidadDescuento = request.getParameter("cantidadDescuento");
		
		int resp = descuentoDao.crearDescuento(tipoDescuento,fechaInicio,fechafin,Double.parseDouble(cantidadDescuento));
		
		if(resp >= 1) {
			System.out.println("Se cre� bien el descuento");
			rd = request.getRequestDispatcher("/vistas/descuento/descuento.jsp");
			rd.forward(request, response);
		}else {
			System.out.println("No se creo el descuento");
			rd = request.getRequestDispatcher("/vistas/descuento/descuento.jsp");
			rd.forward(request, response);
		}
		
	}

}
