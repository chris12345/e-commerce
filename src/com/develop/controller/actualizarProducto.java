package com.develop.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.develop.DAO.ProductoDAO;

/**
 * Servlet implementation class actualizarProducto
 */
@WebServlet("/actualizarProducto")
public class actualizarProducto extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public actualizarProducto() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: Actualizar producto:  ").append(request.getContextPath());
		//ProductoM productM = null;
		System.out.println("Hola get");
		
		//int idProducto = (int) request.getAttribute("idProducto");
		//System.out.println(idProducto);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		System.out.println("Hola post");
		
		ProductoDAO productoDao = new ProductoDAO();
		RequestDispatcher rd;
		
		String idProducto = request.getParameter("id");
		String nombre = request.getParameter("nombre");
		String descripcion = request.getParameter("descripcion");
		String precioUnitario = request.getParameter("precioUnitario");
		String tipoDescuento = request.getParameter("tipoDescuento");
		String tipoCategoria = request.getParameter("tipoCategoria");
		String disponibilidad = request.getParameter("disponibilidad");
		String marca = request.getParameter("marca");
		
		int resultado = productoDao.actualizarProducto(Integer.parseInt(idProducto),nombre,descripcion,Double.parseDouble(precioUnitario),
				tipoDescuento,tipoCategoria,Integer.parseInt(disponibilidad),marca);
		
		if(resultado >= 1) {
			System.out.println("Producto actualizado");
			rd = request.getRequestDispatcher("/vistas/productos/productos.jsp");
			rd.forward(request, response);
			///vistas/productos/productos.jsp
			//request.getContextPath()+"/vistas/productos/productos.jsp"
		}else {
			System.out.println("Error al actualizar");
			rd = request.getRequestDispatcher("/login/login.jsp");
			rd.forward(request, response);
		}
		/*System.out.println(Integer.parseInt(idProducto)); //int
		System.out.println(nombre);
		System.out.println(descripcion);
		System.out.println(precioUnitario); //double
		System.out.println(tipoDescuento);
		System.out.println(tipoCategoria);
		System.out.println(disponibilidad); // int
		System.out.println(marca);*/
		//System.out.println(prod.getNombre());
	}

}
