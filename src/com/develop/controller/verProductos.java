package com.develop.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.ProductoDAO;
import com.develop.models.ProductoM;

/**
 * Servlet implementation class verProductos
 */
@WebServlet("/verProductos")
public class verProductos extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public verProductos() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		ProductoDAO producto = new ProductoDAO();
		ArrayList<ProductoM> prod = producto.verProductos();
		RequestDispatcher rd;
		if(prod != null) {
			System.out.println("Hay contenedor lleno!");
			request.setAttribute("almacenProductos", prod);
			rd = request.getRequestDispatcher("/vistas/productos/productos.jsp");
			rd.forward(request, response);
		}else {
			System.out.println("Mala suerte xc");
			rd = request.getRequestDispatcher("/vistas/main/main.jsp");
			rd.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
	}

}
