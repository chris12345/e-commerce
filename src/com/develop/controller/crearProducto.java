package com.develop.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.ProductoDAO;

/**
 * Servlet implementation class crearProducto
 */
@WebServlet("/crearProducto")
public class crearProducto extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public crearProducto() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// response.getWriter().append("Served at: ").append(request.getContextPath());
		// RequestDispatcher rd;

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// doGet(request, response);
		//String idProducto = request.getParameter("idProducto");
		ProductoDAO productoDao = new ProductoDAO();
		
		RequestDispatcher rd;
		String nombreProducto = request.getParameter("nombre");
		String descripcion = request.getParameter("descripcion");
		double precioUnitario = Double.parseDouble(request.getParameter("precioUnitario"));
		String tipoDescuento = request.getParameter("tipoDescuento");
		String tipoCategoria = request.getParameter("categoria");
		int disponibilidad = Integer.parseInt(request.getParameter("disponibilidad"));
		String marca = request.getParameter("marca");
		
		int respuesta = productoDao.insertarProducto(nombreProducto,descripcion,precioUnitario,tipoDescuento,tipoCategoria,disponibilidad,marca);
		
		if(respuesta >= 1) {
			System.out.println("Creando producto");
			rd = request.getRequestDispatcher("/vistas/productos/productos.jsp");
			rd.forward(request, response);
		}else {
			System.out.println("Error al crear producto");
//			request.getContextPath() 
			rd = request.getRequestDispatcher("/vistas/productos/productos.jsp");
			rd.forward(request, response);
		}
	}

}
