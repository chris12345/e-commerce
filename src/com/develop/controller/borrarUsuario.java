package com.develop.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.UsuarioDAO;

/**
 * Servlet implementation class borrarUsuario
 */
@WebServlet("/borrarUsuario")
public class borrarUsuario extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public borrarUsuario() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		System.out.println("Hola doget");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		//System.out.println("Hola dopost");
		RequestDispatcher rd;
		String idUsuario = request.getParameter("idUsuario");
		UsuarioDAO productoDao = new UsuarioDAO();
		
		int respuesta = productoDao.borrarUsuario(Integer.parseInt(idUsuario));
		
		if(respuesta>=1) {
			System.out.println("Se borro usuario");
			rd = request.getRequestDispatcher("/vistas/usuario/usuario.jsp");
			rd.forward(request, response);
		}else {
			System.out.println("No se borro nada xd");
			rd = request.getRequestDispatcher("/vistas/usuario/usuario.jsp");
			rd.forward(request, response);
		}
		System.out.println(Integer.parseInt(idUsuario));
	}

}
