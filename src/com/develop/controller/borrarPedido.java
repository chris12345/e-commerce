package com.develop.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.PedidoDAO;

/**
 * Servlet implementation class borrarPedido
 */
@WebServlet("/borrarPedido")
public class borrarPedido extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public borrarPedido() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// doGet(request, response);
		PedidoDAO pedidoDao = new PedidoDAO();
		RequestDispatcher rd;
		int respuesta = 0;

		String idPedido = request.getParameter("idPedido");

		respuesta = pedidoDao.borrarPedido(Integer.parseInt(idPedido));

		if (respuesta >= 1) {
			rd = request.getRequestDispatcher("/vistas/pedido/pedido.jsp");
			rd.forward(request, response);
		} else {
			rd = request.getRequestDispatcher("/vistas/pedido/pedido.jsp");
			rd.forward(request, response);
		}
	}

}
