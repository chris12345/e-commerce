package com.develop.controller;

import java.io.IOException;
import java.time.LocalDate;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.PedidoDAO;

/**
 * Servlet implementation class crearPedidoCarrito
 */
@WebServlet("/crearPedidoCarrito")
public class crearPedidoCarrito extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public crearPedidoCarrito() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		System.out.println("XD");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		
		RequestDispatcher rd;
		PedidoDAO pedidoDao = new PedidoDAO();
		
		String nombreProducto = request.getParameter("nombreProducto");
		String precioUnitario = request.getParameter("precioUnitario");
		String cantidad = request.getParameter("cantidad");
		String nombreUsuario = request.getParameter("nombreUsuario");
		
		/*System.out.println("nombreProducto"+nombreProducto);
		System.out.println("precioUnitario: "+precioUnitario);
		System.out.println("cantidad: "+cantidad);
		System.out.println("nombreUsuario: "+nombreUsuario);*/
		
		
		LocalDate fechaCreacion = LocalDate.now();
		LocalDate fechaExpiracion = fechaCreacion.plusDays(7);
		
		char primeraLetra = nombreUsuario.charAt(0);
		String fechaInicioSinGuion = fechaCreacion.toString().replace("-", "");
		
		String referenciaPedido = ""+primeraLetra+fechaInicioSinGuion; 
		String statusCompra = "Creado";
		String statusPedido = "Pendiente";
		
		int respuesta = pedidoDao.crearPedido(referenciaPedido, nombreProducto, Double.parseDouble(precioUnitario),
				Integer.parseInt(cantidad),nombreUsuario, fechaCreacion.toString(), fechaExpiracion.toString(), statusCompra, statusPedido);

		if (respuesta >= 1) {
			System.out.println("Se creo bien el pedido men xd");
			rd = request.getRequestDispatcher("/vistas/main/main.jsp");
			rd.forward(request, response);
		} else {
			System.out.println("No se creo bien el pedido xc");
			rd = request.getRequestDispatcher("/vistas/main/main.jsp");
			rd.forward(request, response);
		}
	}

}
