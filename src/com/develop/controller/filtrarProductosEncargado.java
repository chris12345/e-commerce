package com.develop.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.PedidoDAO;

/**
 * Servlet implementation class filtrarProductosEncargado
 */
@WebServlet("/filtrarProductosEncargado")
public class filtrarProductosEncargado extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public filtrarProductosEncargado() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		String referenciaPedido = request.getParameter("referenciaPedido");
		String statusPedido = request.getParameter("statusPedido");
		RequestDispatcher rd;
		PedidoDAO pedidoDao = new PedidoDAO();
		
		System.out.println("statusPedido: "+statusPedido);
		
		if(pedidoDao!=null) {
			System.out.println("Se filtran XD");
			
			rd = request.getRequestDispatcher("/vistas/main/encargado/encargado.jsp?referenciaPedido="+referenciaPedido+"&statusPedido="+statusPedido);
			rd.forward(request, response);
		} else {
			System.out.println("No filtran xc");
			rd = request.getRequestDispatcher("/vistas/main/encargado/encargado.jsp?referenciaPedido="+referenciaPedido);
			rd.forward(request, response);
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		//System.out.println("Hola dopost");
	}

}
