package com.develop.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.PedidoDAO;

/**
 * Servlet implementation class actualizarPedidoEncargado
 */
@WebServlet("/actualizarPedidoEncargado")
public class actualizarPedidoEncargado extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public actualizarPedidoEncargado() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		String referencia = request.getParameter("referencia");
		String statusPedido = request.getParameter("statusPedido");
		
		RequestDispatcher rd;
		
		PedidoDAO pedidoDao = new PedidoDAO(); 
		
		int resultado = pedidoDao.actualizarPedidoEncargado(referencia, statusPedido);
		if(resultado >= 1) {
			System.out.println("Si se actualiz� xd");
			rd = request.getRequestDispatcher("/vistas/main/encargado/encargado.jsp");
			rd.forward(request, response);
		}else {
			System.out.println("No se actualiz�");
			rd = request.getRequestDispatcher("");
			rd.forward(request, response);
		}
	}
}
