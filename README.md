# e-commerce

Primero que nada este proyecto:
1.- Al iniciar el proyecto, debemos agregar a la URL lo siguiente:
    /login/login.jsp

    quedando así:

    http://localhost:suPuerto/E-comerce/login/login.jsp

2.- Tiene casi toda la funcionalidad solo para el administrador
3.- Valida entre tipos de personal (Administrador, Cliente o Encargado)
4.- Cuando se entra como "Cliente" o "Encargado" solo se topa con una vista banca XD (aun no les creo vistas)

Para el login como Administrador:       <-------
Para la pantalla principal:
1.- Al iniciar la pantalla principal despues del login nos toparemos con una lista ubicada a la izquierda de categorias existentes en la base de datos
2.- En el centro está la lista de productos por categoria (tenemos que seleccionar una categoria para que aparezcan los productos)
3.- Para agregar los productos al carrito debemos especificar la cantidad de productos a comprar
4.- Cuando presionamos el botón de "Agregar", este producto se agregará al carrito de una vez (Puede comprobarlo)

Para la vista de Descuentos:
1.- En esta vista se encuentran los descuentos existentes en la base de datos
2.- En el encabezado de la tabla tenemos un boton de "+ Descuento" para crear un nuevo descuento manualmente como administrador
3.- En cada fila de cada descuento, tenemos 2 botones: "Actualizar" y "Borrar", con las cuales podemos hacer las acciones mencionadas

Para la vista de Pedidos:
1.- Aqui se encuentran la lista de los productos añadidos recientemente con sus atributos correspondientes(se me conplico trabajarla ya que a falta de relaciones y no querer hacer muchos servlets se me hizo facil unir 2 tablas "Compra" y "Pedido", de ahi que existen 2 status diferentes: statusCompra y statusPedido)
2.- En el encabezado de la tabla tenemos un boton de "+ Pedido" para crear un nuevo pedido manualmente como administrador
3.- En cada fila de cada pedido, tenemos 2 botones: "Actualizar" y "Borrar", con las cuales podemos hacer las acciones mencionadas

Para la vista Usuario:
1.- En esta vista se encuentran listados todos los usuarios existentes en la base de datos, con sus respectivos campos llenos
2.- En el encabezado de la tabla tenemos un boton de "+ Usuario" para crear un nuevo usuario manualmente como administrador
3.- En cada fila de cada usuario, tenemos 2 botones: "Actualizar" y "Borrar", con las cuales podemos hacer las acciones mencionadas

Para la vista Categoria:
1.- En esta vista tenemos listados todos los tipos de categorias existentes en la base de datos, con sus respectivos campos llenos
2.- En el encabezado de la tabla tenemos un boton de "+ Categoria" para crear un nuevo usuario manualmente como administrador
3.- En cada fila de cada categoria, tenemos 2 botones: "Actualizar" y "Borrar", con las cuales podemos hacer las acciones mencionadas

Para la vista Productos:
1.- En esta vista tenemos listados todos los productos existentes en la base de datos, con sus respectivos campos llenos
2.- En el encabezado de la tabla tenemos un boton de "+ Producto" para crear un nuevo producto manualmente como administrador
3.- En cada fila de cada categoria, tenemos 2 botones: "Actualizar" y "Borrar", con las cuales podemos hacer las acciones mencionadas
