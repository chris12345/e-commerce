<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
	integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP"
	crossorigin="anonymous">
</head>
<link rel="stylesheet" href="registro.css" />
<body>
	<div class="card container h-100 mt-4 bg-green" style="width: 28rem;">
		<form action="../../../crearProducto" method="post"
			class="container mt-4">
			<div class="mb-3 text-center">
				<label class="form-label">Registro de producto</label>
			</div>
			<div class="mb-3">
				<label for="exampleInputEmail1" class="form-label">Nombre</label> <input
					type="text" class="form-control input_user" name="nombre"
					aria-describedby="emailHelp" placeholder="Nombre producto" required>
			</div>
			<div class="mb-3">
				<label for="exampleInputEmail1" class="form-label">Descripcion
				</label> <input type="text" class="form-control input_user"
					name="descripcion" aria-describedby="emailHelp"
					placeholder="Descripcion" required>
			</div>
			<div class="mb-3">
				<label for="exampleInputEmail1" class="form-label">Precio
					unitario</label> <input type="number" class="form-control input_user"
					name="precioUnitario" aria-describedby="emailHelp"
					placeholder="Precio unitario" required step="0.01">
			</div>
			<div class="mb-3">
				<label for="exampleInputEmail1" class="form-label">Tipo
					descuento</label> <input type="text" class="form-control input_user"
					name="tipoDescuento" aria-describedby="emailHelp"
					placeholder="Tipo descuento" required>
			</div>
			<div class="mb-3">
				<label class="form-label">Categoria</label> <select
					class="form-select form-select-lg mb-3 text-center"
					name="categoria" style="width: 100%;"
					aria-label=".form-select-lg example" required>
					<optionselected>Jamon</option>
					<option value="Jamon">Jamon</option>
					<option value="Jabon">Jabon</option>
					<option value="Sopa">Sopa</option>
				</select>
			</div>
			<div class="mb-3">
				<label for="exampleInputPassword1" class="form-label">Disponibilidad</label>
				<input type="number" class="form-control" name="disponibilidad" required>
			</div>
			<div class="mb-3">
				<label for="exampleInputPassword1" class="form-label">Marca</label>
				<input type="text" class="form-control" name="marca" required>
			</div>
			<div class="mb-3">
				<button class="btn btn-outline-success my-2 my-sm-0 mt-2" name="btnCancelar" id="CerrarSesion"
				type="submit">Crear</button>
			</div>
		</form>
		<form class="form-inline my-2 my-lg-0"
			action="/E-comerce/vistas/productos/productos.jsp">
			<button class="btn btn-outline-danger my-2 my-sm-0 ml-3 mt-2" name="btnCancelar" id="CerrarSesion"
				type="submit">Cancelar</button>
		</form>
	</div>
</body>
</html>