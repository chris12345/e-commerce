<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	<a class="navbar-brand" href="/E-comerce/vistas/main/main.jsp">Tienda</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarSupportedContent"
		aria-controls="navbarSupportedContent" aria-expanded="false"
		aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>

	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item">
				<!--active dentro de class --> <a class="nav-link"
				href="/E-comerce/vistas/descuento/descuento.jsp">Descuentos </a>
			</li>
			<li class="nav-item"><a class="nav-link"
				href="/E-comerce/vistas/pedido/pedido.jsp">Pedidos</a></li>
			<li class="nav-item"><a class="nav-link"
				href="/E-comerce/verUsuarios">Usuarios</a></li>
			<li class="nav-item"><a class="nav-link"
				href="/E-comerce/vistas/categoria/categoria.jsp">Categorias</a></li>
				<li class="nav-item active"><a class="nav-link"
				href="/E-comerce/verProductos">Productos</a></li>
		</ul>
		<form class="form-inline my-2 my-lg-0"
			action="/JavaEEWeb/carrito/carrito.jsp">
			<button class="btn btn-outline-success my-2 my-sm-0" id="VerCarrito"
				type="submit">Ver carrito</button>
		</form>

		<form class="form-inline my-2 my-lg-0" action="/E-comerce/login/login.jsp">
			<button class="btn btn-outline-danger my-2 my-sm-0" id="CerrarSesion"
				type="submit">Cerrar Sesion</button>
		</form>
	</div>
</nav>