<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="com.develop.DAO.ProductoDAO"%>
<%@page import="com.develop.models.ProductoM"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@include file="header.jsp"%>
<%@include file="footer.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Productos</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
	integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP"
	crossorigin="anonymous">
<link rel="stylesheet" href="/E-comerce/vistas/productos/header.css">
<link rel="stylesheet" href="/E-comerce/vistas/productos/footer.css">
</head>
<body>
	<table class="table table-dark table-striped" id="tabla">
		<thead>
			<tr>
				<th scope="col">idProducto</th>
				<th scope="col">Nombre</th>
				<th scope="col">Descripcion</th>
				<th scope="col">Precio unitario</th>
				<th scope="col">Tipo de descuento</th>
				<th scope="col">Tipo de categoria</th>
				<th scope="col">Disponibilidad</th>
				<th scope="col">Marca</th>
				<th scope="col">Fecha de creacion</th>
				<th scope="col"><form class="form-inline my-2 my-lg-0"
						action="<%=request.getContextPath()%>/imprimirProductos">
						<button class="btn btn-outline-light my-2 my-sm-0" id="VerCarrito"
							type="submit">Imprimir lista</button>
					</form></th>
				<th scope="col"><form class="form-inline my-2 my-lg-0"
						action="<%=request.getContextPath()%>/imprimirProductosPDF">
						<button class="btn btn-outline-danger my-2 my-sm-0" id="VerCarrito"
							type="submit">Lista PDF</button>
					</form></th>
				<th Scope="col">
					<form class="form-inline my-2 my-lg-0"
						action="<%=request.getContextPath()%>/vistas/productos/crearProducto/registroProducto.jsp">
						<button class="btn btn-outline-primary my-2 my-sm-0"
							id="VerCarrito" type="submit">+ Producto</button>
					</form>
				</th>
			</tr>
		</thead>
		<%
		ProductoDAO dao = new ProductoDAO();
		List<ProductoM> list = dao.verProductos();
		Iterator<ProductoM> iter = list.iterator();
		ProductoM per = null;
		while (iter.hasNext()) {
			per = iter.next();
		%>
		<tbody>
			<tr>

				<td class="text-center" name="idProducto"><%=per.getIdProducto()%></td>
				<td class="text-center"><%=per.getNombre()%></td>
				<td class="text-center"><%=per.getDescripcion()%></td>
				<td class="text-center"><%=per.getPrecioUnitario()%></td>
				<td class="text-center"><%=per.getTipoDescuento()%></td>
				<td class="text-center"><%=per.getTipoCategoria()%></td>
				<td class="text-center"><%=per.getDisponibilidad()%></td>
				<td class="text-center"><%=per.getMarca()%></td>
				<td class="text-center"><%=per.getFechaCreacion()%></td>
				<td class="text-center"></td>
				<td>
					<form
						action="/E-comerce/vistas/productos/actualizarProducto/actualizarProducto.jsp?id=<%=per.getIdProducto()%>&nombre=<%=per.getNombre()%>&descripcion=<%=per.getDescripcion()%>
					&precioUnitario=<%=per.getPrecioUnitario()%>&tipoDescuento=<%=per.getTipoDescuento()%>&tipoCategoria=<%=per.getTipoCategoria()%>
					&disponibilidad=<%=per.getDisponibilidad()%>&marca=<%=per.getMarca()%>"
						method="post">
						<button type="submit" name="button"
							class="btn btn-outline-warning my-2 my-sm-0">Actualizar</button>
					</form>
				</td>
				<td>
					<form
						action="<%=request.getContextPath()%>/borrarProducto?id=<%=per.getIdProducto()%>"
						method="post">
						<button type="submit" name="button"
							class="btn btn-outline-danger my-2 my-sm-0">Borrar</button>
					</form>
				</td>
			</tr>
			<%
			}
			%>
		</tbody>
	</table>
</body>
</html>