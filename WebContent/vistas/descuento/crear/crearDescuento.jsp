<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
	integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP"
	crossorigin="anonymous">
</head>
<link rel="stylesheet" href="<%=request.getContextPath()%>/vistas/descuento/crear/crearDescuento.css" />
<body>
	<div class="card container h-100 mt-4 bg-green" style="width: 28rem;">
		<form action="<%=request.getContextPath()%>/crearDescuento" method="post" class="container mt-4">
			<div class="mb-3 text-center">
				<label class="form-label">Crear descuento</label>
			</div>
			<div class="mb-3">
				<label for="exampleInputEmail1" class="form-label">Tipo descuento</label> <input
					type="text" class="form-control input_user" name="tipoDescuento"
					aria-describedby="emailHelp" placeholder="Nombre" required>
			</div>
			<div class="mb-3">
				<label for="exampleInputEmail1" class="form-label">Fecha de inicio del descuento (YYYY-MM-DD)</label> <input type="text" class="form-control input_user"
					name="fechaInicio" aria-describedby="emailHelp"
					placeholder="Descripcion" required>
			</div>
			<div class="mb-3">
				<label for="exampleInputEmail1" class="form-label">Fecha fin del descuento (YYYY-MM-DD)</label> <input type="text" class="form-control input_user"
					name="fechafin" aria-describedby="emailHelp"
					placeholder="Descripcion" required>
			</div>
			<div class="mb-3">
				<label for="exampleInputEmail1" class="form-label">Cantidad de descuento</label> <input type="number" class="form-control input_user"
					name="cantidadDescuento" aria-describedby="emailHelp"
					placeholder="Cantidad descuento" step="0.01" required>
			</div>
			<button class="btn btn-outline-success my-2 my-sm-0" id="VerCarrito"
				type="submit">Crear</button>
		</form>
		<form class="form-inline my-2 my-lg-0"
			action="<%=request.getContextPath()%>/vistas/descuento/descuento.jsp">
			<button class="btn btn-outline-danger my-2 my-sm-0 ml-3 mt-2" id="VerCarrito"
				type="submit">Cancelar</button>
		</form>
	</div>
</body>
</html>