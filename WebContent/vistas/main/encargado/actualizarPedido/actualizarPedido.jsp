<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
	integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP"
	crossorigin="anonymous">
</head>
<link rel="stylesheet" href="actualizarPedido.css" />
<body>
	<div class="card container h-100 mt-4 bg-green" style="width: 28rem;">
	<h1><%=request.getParameter("statusPedido") %></h1>
		<form action="<%=request.getContextPath()%>/actualizarPedidoEncargado" method="post" class="container mt-4 ">
			<div class="mb-3 text-center">
				<label class="form-label">Actualizar pedido</label>
			</div>
			<div class="mb-3">
				<label for="exampleInputEmail1" class="form-label">Referencia del pedido</label> <input
					type="text" class="form-control input_user" readonly
					name="referencia" aria-describedby="emailHelp" placeholder="Id"
					value="<%=request.getParameter("referencia")%>">
			</div>
			
			<div class="mb-3">
				<label class="form-label">Status del pedido</label> <select
					class="form-select form-select-lg mb-3 text-center"
					name="statusPedido" style="width: 100%;"
					aria-label=".form-select-lg example">
					<optionselected><%=request.getParameter("statusPedido")%></option>
					<option value="Pendiente">Pendiente</option>
					<option value="Enviado">Enviado</option>
					<option value="Entregado">Entregado</option>
				</select>
			</div>
			
			<button class="btn btn-outline-warning my-2 my-sm-0 mt-2" name="btnCancelar" id="CerrarSesion"
				type="submit">Actualizar</button>
		</form>
		<form class="form-inline my-2 my-lg-0"
			action="<%=request.getContextPath()%>/vistas/main/encargado/encargado.jsp">
			<button class="btn btn-outline-danger my-2 my-sm-0 ml-3 mt-2" name="btnCancelar" id="CerrarSesion"
				type="submit">Cancelar</button>
		</form>
	</div>
</body>
</html>