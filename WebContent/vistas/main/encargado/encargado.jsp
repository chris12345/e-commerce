<%@page import="com.develop.models.PedidoM"%>
<%@page import="com.develop.DAO.PedidoDAO"%>
<%@page import="com.develop.models.ProductoM"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.develop.DAO.ProductoDAO"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.develop.models.CategoriaM"%>
<%@page import="java.util.List"%>
<%@page import="com.develop.DAO.CategoriaDAO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@include file="header.jsp"%>
<%@include file="footer.jsp"%>
<!DOCTYPE html>

<html>
<head>
<meta charset="ISO-8859-1">
<title>Manager page</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
	integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="/E-comerce/vistas/main/encargado/encargado.css">
<link rel="stylesheet"
	href="/E-comerce/vistas/main/encargado/header.css">
<link rel="stylesheet"
	href="/E-comerce/vistas/main/encargado/footer.css">
</head>
<body>
	<div id="contenedor">
		<div id="izquierdo">
			<table class="table table-dark table-striped mt-2" id="tabla">
				<thead>
					<tr>
						<th scope="col">Pedidos pagados y sin concluir</th>
					</tr>
				</thead>
				<%
				PedidoDAO pedidoDao = new PedidoDAO();
				List<PedidoM> lista123 = pedidoDao.filtroParaEncargado();
				Iterator<PedidoM> iter = lista123.iterator();
				PedidoM miPedido = null;
				while (iter.hasNext()) {
					miPedido = iter.next();
				%> 
				
				<tbody>
					<tr>
						<td><a
							href="<%=request.getContextPath()%>/filtrarProductosEncargado?referenciaPedido=<%=miPedido.getReferenciaPedido()%>
							&statusPedido=<%=miPedido.getStatusPedido()%>"><%=miPedido.getReferenciaPedido()%></a>
							
						</td>
					</tr>
					<%
					}
					%>
				</tbody>
			</table>
		</div>
		<div id="centro">

			<table class="table table-dark table-striped" id="tabla">
				<thead>
					<tr>
						<th scope="col">Nombre de producto</th>
						<th scope="col">Precio unitario</th>
						<th scope="col">Cantidad</th>
						<th scope="col">
							<form
								action="<%=request.getContextPath()%>/vistas/main/encargado/actualizarPedido/actualizarPedido.jsp?
								referencia=<%=request.getParameter("referenciaPedido")%>&statusPedido=<%=request.getParameter("statusPedido")%>"
								method="post">
								<button type="submit" name="button"
									class="btn btn-outline-warning my-2 my-sm-0">Modificar
									Status</button>
							</form>
						</th>
					</tr>
				</thead>
				<%
				PedidoDAO pedidoDao2 = new PedidoDAO();
				List<PedidoM> pedidoLista = pedidoDao2.filtroProductoEncargado(request.getParameter("referenciaPedido"));
				//out.println(request.getParameter("tipoCategoria"));
				//out.println(lista2.toString());
				for (PedidoM listaPedido : pedidoLista) {
					
				%>
				<tbody>

					<tr>
						<td><%=listaPedido.getNombreProducto()%></td>
						<td><%=listaPedido.getPrecioUnitario()%></td>
						<td><%=listaPedido.getCantidad()%></td>
						<td></td>
					</tr>
					<%
					}
					%>
					
					<div>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>