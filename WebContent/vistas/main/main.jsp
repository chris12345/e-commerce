<%@page import="com.develop.models.ProductoM"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.develop.DAO.ProductoDAO"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.develop.models.CategoriaM"%>
<%@page import="java.util.List"%>
<%@page import="com.develop.DAO.CategoriaDAO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@include file="header.jsp"%>
<%@include file="footer.jsp"%>
<!DOCTYPE html>

<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
	integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP"
	crossorigin="anonymous">
<link rel="stylesheet" href="/E-comerce/vistas/main/main.css">
<link rel="stylesheet" href="/E-comerce/vistas/main/header.css">
<link rel="stylesheet" href="/E-comerce/vistas/main/footer.css">
</head>
<body>
	<div id="contenedor">
		<div id="izquierdo">
			<table class="table table-dark table-striped mt-2" id="tabla">
				<thead>
					<tr>
						<th scope="col">Categorias</th>
					</tr>
				</thead>
				<%
				CategoriaDAO categoriaDao = new CategoriaDAO();
				List<CategoriaM> lista = categoriaDao.verCategorias();
				Iterator<CategoriaM> iter = lista.iterator();
				CategoriaM miCategoria = null;
				while (iter.hasNext()) {
					miCategoria = iter.next();
				%>
				<tbody>
					<tr>
						<td><a
							href="<%=request.getContextPath()%>/filtrarProductos?tipoCategoria=<%=miCategoria.getTipoCategoria()%>&tipoUsuario=${tipoSession}"><%=miCategoria.getTipoCategoria()%></a>
						</td>
					</tr>
					<%
					}
					%>
				</tbody>
			</table>
		</div>
		<div id="centro">
		
			<table class="table table-dark table-striped" id="tabla">
				<thead>
					<tr>
						<th scope="col">Nombre</th>
						<th scope="col">Descripcion</th>
						<th scope="col">Precio unitario</th>
						<th scope="col">Cantidad</th>
						
					</tr>
				</thead>
				<%
				ProductoDAO productoDao = new ProductoDAO();
				List<ProductoM> lista2 = productoDao.filtrarProducto(request.getParameter("tipoCategoria"));
				//out.println(request.getParameter("tipoCategoria"));
				//out.println(lista2.toString());
				for (ProductoM listaProducto : lista2) {
				%>
				<tbody>
					<tr>
						<td><%=listaProducto.getNombre()%></td>
						<td><%=listaProducto.getDescripcion()%></td>
						<td>$<%=listaProducto.getPrecioUnitario()%></td>
						
						<td>
							<form class="form-inline my-2 my-lg-0"
							action="<%=request.getContextPath()%>/crearPedidoCarrito?nombreProducto=<%=listaProducto.getNombre()%>
							&precioUnitario=<%=listaProducto.getPrecioUnitario()%>&nombreUsuario=<%=request.getSession().getAttribute("usuarioSession").toString()%>"
							method="post">
							
							<input type="number" name="cantidad" min="1" max="99" required>  --->   
							
							<button class="btn btn-outline-primary my-2 my-sm-0"
								id="agregar" type="submit">Agregar</button>
							</form>
						</td>
					</tr>
					<%
					}
					%>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>