<%@page import="java.time.LocalDate"%>
<%@page import="java.time.temporal.ChronoUnit"%>
<%@page import="com.develop.models.DescuentoM"%>
<%@page import="com.develop.DAO.DescuentoDAO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.develop.models.CategoriaM"%>
<%@page import="com.develop.DAO.CategoriaDAO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ include file="headerDescuento.jsp" %>
    <%@include file="footer.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
	integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP"
	crossorigin="anonymous">
</head>
<link rel="stylesheet" href="/E-comerce/vistas/categoria/headerCategoria.css">
<link rel="stylesheet" href="/E-comerce/vistas/descuento/footer.css">
<body>
	<table class="table table-dark table-striped" id="tabla">
		<thead>
			<tr>
				
				<th scope="col">Tipo descuento</th>
				<th scope="col">Fecha inicio</th>
				<th scope="col">Fecha fin</th>
				<th scope="col">Cantidad descuento</th>
				<th scope="col">Tiempo restante (dias)</th>

				
			</tr>
		</thead>
		<%
		DescuentoDAO descuentoDao = new DescuentoDAO();
		ArrayList<DescuentoM> contenedor = descuentoDao.verDescuento();
		
		Iterator<DescuentoM> iter = contenedor.iterator();
		DescuentoM descuento = null;
		while(iter.hasNext()){
			descuento = iter.next();
		%>
		<tbody>
			<tr>
				<td class="text-center"><%=descuento.getTipoDescuento()%></td>
				<td class="text-center"><%=descuento.getFechaInicio()%></td>
				<td class="text-center"><%=descuento.getFechaFin()%></td>
				<td class="text-center">$<%=descuento.getDescuento()%></td>
				<td class="text-center"><%
				String mensaje ="";
						LocalDate fechaInicio = LocalDate.parse(descuento.getFechaInicio().toString());
						LocalDate fechaTermino = LocalDate.parse(descuento.getFechaFin().toString());
						long numeroDias = ChronoUnit.DAYS.between(fechaInicio, fechaTermino);
						if(numeroDias >= 0){
							mensaje=""+numeroDias;
						}else{
							mensaje="La fecha de termino es menor a la de inicio: "+numeroDias;
						}
				%><%=mensaje%></td>
			</tr>
			<%
		}
			%>
		</tbody>
	</table>
</body>
</html>