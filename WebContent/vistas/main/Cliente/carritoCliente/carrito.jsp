<%@page import="java.util.Iterator"%>
<%@page import="com.develop.models.PedidoM"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.develop.DAO.PedidoDAO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
	integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP"
	crossorigin="anonymous">
<link rel="stylesheet" href="/E-comerce/vistas/main/Cliente/carritoCliente/carrito.css">
</head>
<body>
	<table class="table table-dark table-striped text-center" id="tabla">
		<thead>
			<tr>
				<th scope="col">Producto</th>
				<th scope="col">Precio unitario</th>
				<th scope="col">Cantidad</th>
				<th scope="col">Total a pagar</th>
			</tr>
		</thead>
		<%
		PedidoDAO pedidoDao = new PedidoDAO();
		ArrayList<PedidoM> contenedor = pedidoDao.filtrarPedido(request.getSession().getAttribute("usuarioSession").toString());

		Iterator<PedidoM> iter = contenedor.iterator();
		PedidoM pedido = null;
		while (iter.hasNext()) {
			pedido = iter.next();
		%>
		<tbody>
			<tr>
				<td class="text-center"><%=pedido.getNombreProducto()%></td>
				<td class="text-center"><%=pedido.getPrecioUnitario()%></td>
				<td class="text-center"><%=pedido.getCantidad()%></td>
				<td class="text-center"><%=pedido.getPrecioUnitario() * pedido.getCantidad()%></td>
				<td>
					<form action="<%=request.getContextPath()%>/borrarPedidoCarrito?idPedido=<%=pedido.getIdPedido()%>" method="post">
						<button type="submit" name="button"
							class="btn btn-outline-danger my-2 my-sm-0">Quitar</button>
					</form>
				</td>
			</tr>

			<%
			}
			%>




		</tbody>
		<form action="<%=request.getContextPath()%>/actualizarPedidoCarrito?nombreUsuario=<%=request.getSession().getAttribute("usuarioSession").toString()%>" method="post">
			<button type="submit" name="button"
				class="btn btn-outline-warning my-2 my-sm-0">Pagar</button>
		</form>
		<form action="<%=request.getContextPath()%>/vistas/main/Cliente/main.jsp"
			method="post">
			<button type="submit" name="button"
				class="btn btn-outline-primary my-2 my-sm-0">Seguir
				comprando</button>
		</form>
		<form action="<%=request.getContextPath()%>/cancelarPedidoCarrito?nombreUsuario=<%=request.getSession().getAttribute("usuarioSession").toString()%>" method="post">
			<button type="submit" name="button"
				class="btn btn-outline-danger my-2 my-sm-0">Cancelar pedido</button>
		</form>
</body>
</html>