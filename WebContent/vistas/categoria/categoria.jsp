<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.develop.models.CategoriaM"%>
<%@page import="com.develop.DAO.CategoriaDAO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ include file="headerCategoria.jsp" %>
    <%@include file="footer.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
	integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP"
	crossorigin="anonymous">
</head>
<link rel="stylesheet" href="/E-comerce/vistas/categoria/headerCategoria.css">
<link rel="stylesheet" href="/E-comerce/vistas/categoria/footer.css">
<body>
	<table class="table table-dark table-striped" id="tabla">
		<thead>
			<tr>
				<th scope="col">idCategoria</th>
				<th scope="col">Tipo categoria</th>
				<th scope="col">Descripcion categoria</th>
				<th scope="col"></th>
				<th Scope="col">
					<form class="form-inline my-2 my-lg-0"
						action="<%=request.getContextPath()%>/vistas/categoria/crearCategoria/crearCategoria.jsp">
						<button class="btn btn-outline-primary my-2 my-sm-0"
							id="VerCarrito" type="submit">+ Categoria</button>
					</form>
				</th>
			</tr>
		</thead>
		<%
		CategoriaDAO categoriaDao = new CategoriaDAO();
		ArrayList<CategoriaM> contenedor = categoriaDao.verCategorias();
		
		Iterator<CategoriaM> iter = contenedor.iterator();
		CategoriaM categoria = null;
		while(iter.hasNext()){
			categoria = iter.next();
		%>
		<tbody>
			<tr>

				<td class="text-center"><%=categoria.getIdCategoria()%></td>
				<td class="text-center"><%=categoria.getTipoCategoria()%></td>
				<td class="text-center"><%=categoria.getDescripcionCategoria()%></td>
				<td class="text-center"></td>
				<td> 
					<form
						action="/E-comerce/vistas/categoria/actualizarCategoria/actualizarCategoria.jsp?idCategoria=<%=categoria.getIdCategoria()%>&tipoCategoria=<%=categoria.getTipoCategoria()%>&descripcionCategoria=<%=categoria.getDescripcionCategoria()%>"
						method="post">
						<button type="submit" name="button"
							class="btn btn-outline-warning my-2 my-sm-0">Actualizar</button>
					</form>
				</td>
				<td>
					<form
						action="<%= request.getContextPath()%>/borrarCategoria?idCategoria=<%=categoria.getIdCategoria()%>"
						method="post">
						<button type="submit" name="button"
							class="btn btn-outline-danger my-2 my-sm-0">Borrar</button>
					</form> 
				</td>
			</tr>
			<%
		}
			%>
		</tbody>
	</table>
</body>
</html>