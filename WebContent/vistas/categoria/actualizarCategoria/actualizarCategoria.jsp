<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
	integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP"
	crossorigin="anonymous">
</head>
<link rel="stylesheet" href="<%=request.getContextPath()%>/vistas/categoria/actualizarCategoria/actualizarCategoria.css" />
<body>
	<div class="card container h-100 mt-4 bg-green" style="width: 28rem;">
		<form action="<%=request.getContextPath()%>/actualizarCategoria" method="post" class="container mt-4">
			<div class="mb-3 text-center">
				<label class="form-label">Actualizar categoria</label>
			</div>
			<div class="mb-3">
				<label for="exampleInputEmail1" class="form-label">Id categoria</label> <input
					type="text" class="form-control input_user" name="idCategoria"
					aria-describedby="emailHelp" placeholder="Nombre" required readonly
					value="<%=request.getParameter("idCategoria")%>">
			</div>
			<div class="mb-3">
				<label for="exampleInputEmail1" class="form-label">Tipo categoria</label> <input
					type="text" class="form-control input_user" name="tipoCategoria"
					aria-describedby="emailHelp" placeholder="Nombre" required
					value="<%=request.getParameter("tipoCategoria")%>">
			</div>
			<div class="mb-3">
				<label for="exampleInputEmail1" class="form-label">Descripcion categoria</label> 
				<input type="text" class="form-control input_user"
					name="descripcionCategoria" aria-describedby="emailHelp"
					placeholder="Apellido paterno" required
					value="<%=request.getParameter("descripcionCategoria")%>">
			</div>
			
			<button class="btn btn-outline-warning my-2 my-sm-0" id="VerCarrito"
				type="submit">Actualizar</button>
		</form>
		<form class="form-inline my-2 my-lg-0"
			action="<%=request.getContextPath()%>/vistas/categoria/categoria.jsp">
			<button class="btn btn-outline-danger my-2 my-sm-0 ml-3 mt-2" id="VerCarrito"
				type="submit">Cancelar</button>
		</form>
	</div>
</body>
</html>