<%@page import="java.util.Iterator"%>
<%@page import="com.develop.models.UsuarioM"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.develop.DAO.UsuarioDAO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@include file="header.jsp"%>
<%@include file="footer.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
	integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP"
	crossorigin="anonymous">
</head>
<link rel="stylesheet" href="/E-comerce/vistas/usuario/header.css">
<link rel="stylesheet" href="/E-comerce/vistas/usuario/footer.css">
<body>
	<table class="table table-dark table-striped" id="tabla">
		<thead>
			<tr>
				<th scope="col">idUsuario</th>
				<th scope="col">Nombre</th>
				<th scope="col">Apellido paterno</th>
				<th scope="col">Apellido materno</th>
				<th scope="col">Direccion</th>
				<th scope="col">Password</th>
				<th scope="col">Tipo de usuario</th>
				<th scope="col">Telefono</th>
				<th scope="col">Email</th>
				<th scope="col">Codigo postal</th>
				<th scope="col">Ciudad</th>
				<th scope="col">
					<form class="form-inline my-2 my-lg-0"
						action="<%=request.getContextPath()%>/imprimirUsuarios">
						<button class="btn btn-outline-light my-2 my-sm-0"
							id="VerCarrito" type="submit">Lista txt</button>
					</form>
				</th>
				<th scope="col">
					<form class="form-inline my-2 my-lg-0"
						action="<%=request.getContextPath()%>/listaUsuarioPDF">
						<button class="btn btn-outline-danger my-2 my-sm-0"
							id="VerCarrito" type="submit">Lista PDF</button>
					</form>
				</th>
				<th Scope="col">
					<form class="form-inline my-2 my-lg-0"
						action="<%=request.getContextPath()%>/vistas/usuario/crearUsuario/crearUsuario.jsp">
						<button class="btn btn-outline-primary my-2 my-sm-0"
							id="VerCarrito" type="submit">+ Usuario</button>
					</form>
				</th>
			</tr>
		</thead>
		<%
		UsuarioDAO usuarioDao = new UsuarioDAO();
		List<UsuarioM> contenedor = usuarioDao.verUsuario();
		Iterator<UsuarioM> iter = contenedor.iterator();
		UsuarioM usuario = null;
		while (iter.hasNext()) {
			usuario = iter.next();
		%>
		<tbody>
			<tr>

				<td class="text-center"><%=usuario.getIdUsuario()%></td>
				<td class="text-center"><%=usuario.getNombre()%></td>
				<td class="text-center"><%=usuario.getApellidoPaterno()%></td>
				<td class="text-center"><%=usuario.getApellidoMaterno()%></td>
				<td class="text-center"><%=usuario.getDireccion()%></td>
				<td class="text-center"><%=usuario.getPassword()%></td>
				<td class="text-center"><%=usuario.getTipoUsuario()%></td>
				<td class="text-center"><%=usuario.getTelefono()%></td>
				<td class="text-center"><%=usuario.getEmail()%></td>
				<td class="text-center"><%=usuario.getCodigoPostal()%></td>
				<td class="text-center"><%=usuario.getCiudad()%></td>
				<td class="text-center"></td>
<!-- 				<td class="text-center"></td> -->
				<td>
					<form
						action="/E-comerce/vistas/usuario/actualizarUsuario/actualizarUsuario.jsp?idUsuario=<%=usuario.getIdUsuario()%>
						&nombre=<%=usuario.getNombre()%>&apellidoPaterno=<%=usuario.getApellidoPaterno()%>&apellidoMaterno=<%=usuario.getApellidoMaterno()%>
						&direccion=<%=usuario.getDireccion()%>&password=<%=usuario.getPassword()%>&tipoUsuario=<%=usuario.getTipoUsuario()%>
						&telefono=<%=usuario.getTelefono()%>
						&email=<%=usuario.getEmail()%>&cp=<%=usuario.getCodigoPostal()%>&ciudad=<%=usuario.getCiudad()%>"
						method="post">
						<button type="submit" name="button"
							class="btn btn-outline-warning my-2 my-sm-0">Actualizar</button>
					</form>
				</td>
				<td>
					<form
						action="<%=request.getContextPath()%>/borrarUsuario?idUsuario=<%=usuario.getIdUsuario()%>"
						method="post">
						<button type="submit" name="button"
							class="btn btn-outline-danger my-2 my-sm-0">Borrar</button>
					</form>
				</td>
			</tr>
			<%
			}
			%>
		</tbody>
	</table>
</body>
</html>