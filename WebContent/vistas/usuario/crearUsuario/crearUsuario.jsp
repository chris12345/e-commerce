<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
	integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP"
	crossorigin="anonymous">
</head>
<link rel="stylesheet" href="registro.css" />
<body>
	<div class="card container h-100 mt-4 bg-green" style="width: 28rem;">
		<form action="<%=request.getContextPath()%>/crearUsuario" method="post" class="container mt-4">
			<div class="mb-3 text-center">
				<label class="form-label">Registro de usuario</label>
			</div>
			<div class="mb-3">
				<label for="exampleInputEmail1" class="form-label">Nombre</label> <input
					type="text" class="form-control input_user" name="nombre"
					aria-describedby="emailHelp" placeholder="Nombre" required>
			</div>
			<div class="mb-3">
				<label for="exampleInputEmail1" class="form-label">Apellido
					paterno</label> <input type="text" class="form-control input_user"
					name="apellidoPaterno" aria-describedby="emailHelp"
					placeholder="Apellido paterno" required>
			</div>
			<div class="mb-3">
				<label for="exampleInputEmail1" class="form-label">Apellido
					materno</label> <input type="text" class="form-control input_user"
					name="apellidoMaterno" aria-describedby="emailHelp"
					placeholder="Apellido materno" required>
			</div>
			<div class="mb-3">
				<label for="exampleInputEmail1" class="form-label">Direccion</label>
				<input type="text" class="form-control input_user" name="direccion"
					aria-describedby="emailHelp" placeholder="Direccion" required>
			</div>
			<div class="mb-3">
				<label for="exampleInputPassword1" class="form-label">Contraseņa</label>
				<input type="password" class="form-control" name="password" required>
			</div>
			<div class="mb-3">
				<label class="form-label">Selecciona el tipo de usuario</label> <select
					class="form-select form-select-lg mb-3 text-center"
					name="tipoUsuario" style="width: 100%;"
					aria-label=".form-select-lg example" required>
					<optionselected>Cliente</option>
					<option value="Cliente">Cliente</option>
					<option value="Administrador">Administrador</option>
					<option value="Encargado">Encargado</option>
				</select required>
			</div>
			<div class="mb-3">
				<label for="exampleInputPassword1" class="form-label">Telefono</label>
				<input type="text" class="form-control" name="telefono" required>
			</div>
			<div class="mb-3">
				<label for="exampleInputPassword1" class="form-label" required>Email</label>
				<input type="text" class="form-control" name="email" required>
			</div>
			<div class="mb-3">
				<label for="exampleInputPassword1" class="form-label">Codigo
					Postal</label> <input type="text" class="form-control" name="cp" required>
			</div>
			<div class="mb-3">
				<label for="exampleInputPassword1" class="form-label">Ciudad</label>
				<input type="text" class="form-control" name="ciudad" required>
			</div>
			<button class="btn btn-outline-success my-2 my-sm-0" id="VerCarrito"
				type="submit">Crear</button>
		</form>
		<form class="form-inline my-2 my-lg-0"
			action="<%=request.getContextPath()%>/vistas/usuario/usuario.jsp">
			<button class="btn btn-outline-danger my-2 my-sm-0 ml-3 mt-2" id="VerCarrito"
				type="submit">Cancelar</button>
		</form>
	</div>
</body>
</html>