<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
	integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP"
	crossorigin="anonymous">
</head>
<link rel="stylesheet" href="registro.css" />
<body>
	<div class="card container h-100 mt-4 bg-green" style="width: 28rem;">
		<form action="<%=request.getContextPath()%>/actualizarPedido" method="post" class="container mt-4 ">
			<div class="mb-3 text-center">
				<label class="form-label">Actualizar pedido</label>
			</div>
			<div class="mb-3">
				<label for="exampleInputEmail1" class="form-label">Id pedido</label> <input
					type="text" class="form-control input_user" name="idPedido" readonly="readonly"
					aria-describedby="emailHelp" placeholder="idPedido" required
					value="<%=request.getParameter("idPedido")%>">
			</div>
			<div class="mb-3">
				<label for="exampleInputEmail1" class="form-label">Referencia del pedido
				</label> <input type="text" class="form-control input_user"
					name="referenciaPedido" aria-describedby="emailHelp"
					placeholder="Referencia"
					value="<%=request.getParameter("referenciaPedido")%>" required>
			</div>
			<div class="mb-3">
				<label for="exampleInputEmail1" class="form-label">Nombre del producto
				</label> <input type="text" class="form-control input_user"
					name="nombreProducto" aria-describedby="emailHelp"
					placeholder="Nombre del producto"
					value="<%=request.getParameter("nombreProducto")%>" required>
			</div>
			<div class="mb-3">
				<label for="exampleInputEmail1" class="form-label">Precio unitario
				</label> <input type="number" class="form-control input_user" step="0.01"
					name="precioUnitario" aria-describedby="emailHelp"
					placeholder="Precio unitario"
					value="<%=request.getParameter("precioUnitario")%>" required>
			</div>
			<div class="mb-3">
				<label for="exampleInputEmail1" class="form-label">Cantidad
				</label> <input type="number" class="form-control input_user"
					name="cantidad" aria-describedby="emailHelp"
					placeholder="Cantidad"
					value="<%=request.getParameter("cantidad")%>" required>
			</div>
			<div class="mb-3">
				<label for="exampleInputEmail1" class="form-label">Usuario</label> <input type="text"
					class="form-control input_user" name="nombreUsuario"
					aria-describedby="emailHelp" placeholder="Nombre de usuario" readonly="readonly"
					value="<%=request.getParameter("nombreUsuario")%>" required>
			</div>
			<div class="mb-3">
				<label for="exampleInputEmail1" class="form-label">Fecha de creacion</label> <input type="text" class="form-control input_user"
					name="fechaCreacion" aria-describedby="emailHelp"
					placeholder="Fecha de creacion"
					value="<%=request.getParameter("fechaCreacion")%>" required>
			</div>
			<div class="mb-3">
				<label for="exampleInputPassword1" class="form-label">Fecha de expiracion</label>
				<input type="number" class="form-control" name="fechaExpiracion"
					value="<%=request.getParameter("fechaExpiracion")%>" required>
			</div>
			<div class="mb-3">
				<label class="form-label">Status de compra</label> <select
					class="form-select form-select-lg mb-3 text-center" name="statusCompra"
					style="width: 100%;" aria-label=".form-select-lg example">
					<optionselected><%=request.getParameter("statusCompra")%></option>
					<option value="Creado">Creado</option>
					<option value="Pagado">Pagado</option>
				</select>
			</div>
			<div class="mb-3">
				<label class="form-label">Status de pedido</label> <select
					class="form-select form-select-lg mb-3 text-center" name="statusPedido"
					style="width: 100%;" aria-label=".form-select-lg example">
					<optionselected><%=request.getParameter("statusPedido")%></option>
					<option value="Pendiente">Pendiente</option>
					<option value="Enviado">Enviado</option>
					<option value="Entregado">Entregado</option>
				</select>
			</div>
			<button class="btn btn-outline-warning my-2 my-sm-0 mt-2" name="btnCancelar" id="CerrarSesion"
				type="submit">Actualizar</button>
		</form>
		<form class="form-inline my-2 my-lg-0"
			action="/E-comerce/vistas/pedido/pedido.jsp">
			<button class="btn btn-outline-danger my-2 my-sm-0 ml-3 mt-2" name="btnCancelar" id="CerrarSesion"
				type="submit">Cancelar</button>
		</form>
	</div>
</body>
</html>