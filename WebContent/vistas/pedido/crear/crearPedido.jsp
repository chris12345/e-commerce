<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
	integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP"
	crossorigin="anonymous">
</head>
<link rel="stylesheet" href="<%=request.getContextPath()%>/vistas/pedido/crear/crearPedido.css" />
<body>
	<div class="card container h-100 mt-4 bg-green" style="width: 28rem;">
		<form action="<%=request.getContextPath()%>/crearPedido" method="post" class="container mt-4">
			<div class="mb-3 text-center">
				<label class="form-label">Crear pedido</label>
			</div>
			<div class="mb-3">
				<label for="exampleInputEmail1" class="form-label">Nombre usuario</label> <input
					type="text" class="form-control input_user" name="nombreUsuario" readonly
					value="${usuarioSession}"
					aria-describedby="emailHelp" placeholder="Nombre de usuario" required>
			</div>
			<div class="mb-3">
				<label for="exampleInputEmail1" class="form-label">Nombre producto</label> <input
					type="text" class="form-control input_user" name="nombreProducto"
					aria-describedby="emailHelp" placeholder="Nombre de producto" required>
			</div>
			<div class="mb-3">
				<label for="exampleInputEmail1" class="form-label">Precio Unitario</label> <input
					type="number" class="form-control input_user" name="precioUnitario"
					aria-describedby="emailHelp" placeholder="Precio unitario" step="0.01" required>
			</div>
			<div class="mb-3">
				<label for="exampleInputEmail1" class="form-label">Cantidad</label> <input type="number" class="form-control input_user"
					name="cantidad" aria-describedby="emailHelp"
					placeholder="Cantidad de unidades" required>
			</div>
			<button class="btn btn-outline-success my-2 my-sm-0" id="VerCarrito"
				type="submit">Crear</button>
		</form>
		<form class="form-inline my-2 my-lg-0"
			action="<%=request.getContextPath()%>/vistas/pedido/pedido.jsp">
			<button class="btn btn-outline-danger my-2 my-sm-0 ml-3 mt-2" id="VerCarrito"
				type="submit">Cancelar</button>
		</form>
	</div>
</body>
</html>