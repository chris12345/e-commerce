<%@page import="com.develop.models.PedidoM"%>
<%@page import="com.develop.DAO.PedidoDAO"%>
<%@page import="java.time.temporal.ChronoUnit"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ include file="headerPedido.jsp" %>
    <%@include file="footer.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
	integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP"
	crossorigin="anonymous">
</head>
<link rel="stylesheet" href="/E-comerce/vistas/categoria/headerCategoria.css">
<link rel="stylesheet" href="/E-comerce/vistas/pedido/footer.css">
<body>
	<table class="table table-dark table-striped" id="tabla">
		<thead>
			<tr>
				<th scope="col">Id pedido</th>
				<th scope="col">Referencia</th>
				<th scope="col">Nombre producto</th>
				<th scope="col">Precio unitario</th>
				<th scope="col">Cantidad</th>
				<th scope="col">Usuario</th>
				<th scope="col">Fecha creacion</th>
				<th scope="col">Fecha expiracion</th>
				<th scope="col">Status compra</th>
				<th scope="col">Status pedido</th>
				<th scope="col"></th>
				<th Scope="col">
					<form class="form-inline my-2 my-lg-0"
						action="<%=request.getContextPath()%>/vistas/pedido/crear/crearPedido.jsp">
						<button class="btn btn-outline-primary my-2 my-sm-0"
							id="VerCarrito" type="submit">+ Pedido</button>
					</form>
				</th>
			</tr>
		</thead>
		
		<%
			PedidoDAO pedidoDao = new PedidoDAO();
			ArrayList<PedidoM> contenedor = pedidoDao.verPedido();
		Iterator<PedidoM> iter = contenedor.iterator();
		PedidoM pedido = null;
		while(iter.hasNext()){
			pedido=iter.next();
		
		%>
		<tbody>
			<tr>
				<td class="text-center"><%=pedido.getIdPedido() %></td>
				<td class="text-center"><%=pedido.getReferenciaPedido()%></td>
				<td class="text-center"><%=pedido.getNombreProducto()%></td>
				<td class="text-center">$<%=pedido.getPrecioUnitario()%></td>
				<td class="text-center"><%=pedido.getCantidad()%></td>
				<td class="text-center"><%=pedido.getNombreUsuario()%></td>
				<td class="text-center"><%=pedido.getFechaCreacion()%></td>
				<td class="text-center"><%=pedido.getFechaExpiracion()%></td>
				<td class="text-center"><%=pedido.getStatusCompra()%></td>
				<td class="text-center"><%=pedido.getStatusPedido()%></td>
				<td class="text-center"></td>
				<td> 
					<form
						action="/E-comerce/vistas/pedido/actualizar/actualizarPedido.jsp?idPedido=<%=pedido.getIdPedido()%>
						&referenciaPedido=<%=pedido.getReferenciaPedido()%>&nombreProducto=<%=pedido.getNombreProducto()%>
						&precioUnitario=<%=pedido.getPrecioUnitario()%>&cantidad=<%=pedido.getCantidad()%>
						&nombreUsuario=<%=pedido.getNombreUsuario()%>&fechaCreacion=<%=pedido.getFechaCreacion()%>
						&fechaExpiracion=<%=pedido.getFechaExpiracion()%>&statusCompra=<%=pedido.getStatusCompra()%>
						&statusPedido=<%=pedido.getStatusPedido()%>"
						method="post">
						<button type="submit" name="button"
							class="btn btn-outline-warning my-2 my-sm-0">Actualizar</button>
					</form>
				</td>
				<td>
					<form
						action="<%=request.getContextPath()%>/borrarPedido?idPedido=<%=pedido.getIdPedido()%>"
						method="post">
						<button type="submit" name="button"
							class="btn btn-outline-danger my-2 my-sm-0">Borrar</button>
					</form> 
				</td>
			</tr>
			<%
		}
			%>
			
		</tbody>
	</table>
</body>
</html>